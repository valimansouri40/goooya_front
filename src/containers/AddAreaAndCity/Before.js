import axios from 'axios'
import  { apidomain, cookiejwt } from '../../store/utility/cookie';

export const BeforeCityAndArea = async(route, query)=>{
        let data;
        await axios(apidomain+ '/realstate/'+ route+ "?"+ query,  {
            method:'post',
            data:data,
            headers:{ 'Authorization': `Bearer ${cookiejwt}`}
        }).then(res=>{
            // console.log(res.data.data);
            data = res.data.data;
        }).catch(er=>{
            console.clear()
        });
        return data;
}

