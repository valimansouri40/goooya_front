import React,{useEffect, useState} from "react";
import { connect } from "react-redux";
import "./SearchResult.css";
import * as action from '../../store/action/index';
import Header from '../../components/Header/Header';
import Footer from "../../components/Footer/Footer";
import SearchRs from "../../components/SearchResult/SearchResult";
import CloseComponent from "../../components/CloseComponent/CloseComponent";

const SearchResult= (props)=>{
        const {areaall,cityall,length,   addMarkinit, lessmarkinit, 
             getallfilterinit,AllData, changefilehandller,filter, auth, sendreq, REALSTATEGETALLINIT}= props;
            
            const [page, setpage]=useState(1)
            const [tab, settab]=useState('');
            const [show, setShow] = useState(false);
            

            // window.addEventListener('resize',()=>{
                
            //     if(window.innerWidth > 700){setShow(true);}
            // })
        
            
        const [data, setdata] = useState({
            Tipic: "sells",
            City: "",
            Area: "",
            SomeRoom: "",
            TypeState: "",
            Mortgage: "",
            Lease:"",
            YearBuild: "",
            Measure: "",
            Parking: false,
            OfStorage: false,
            Pasio: false,
            Pool: false,
            Security: false,
            Sona: false,
            Assansor: false
        })
       

        useEffect(()=>{
            changefilehandller("", 'getallcity','')
        },[])
        useEffect(()=>{
            
                getallfilterinit(data.Tipic === "rahn"?'rh':'sell');
                
        },[data])
       
        const setvalueHandller =(value, id)=>{
           
            let spobj = {...data};
            if(value === 'rahn' && value === 'sells'){
               spobj.Mortgage = '';
               spobj.Lease = '';
            }
            

            spobj = {
                ...data,
                [id]:value
            }

            setdata(spobj)
        }

        const searchquery = window.location.hash;
            
            useEffect(()=>{
                
                if(searchquery.split("&").includes("Tipic=rahn")){
                    setvalueHandller('rahn', 'Tipic');
                }else{
                   setvalueHandller('sells', 'Tipic')
                }
            },[searchquery])
        let userid= null;
        if(auth){
            userid= auth._id
        }
        const searchhandller=(e)=>{
            localStorage.setItem('searchquery',Number(localStorage.getItem('searchquery')) + 1);
    
            const obj= Object.entries(data);
              
            let st= '';
            obj.map((mp,i)=>{
                if(mp[0] === 'Measure' || mp[0] ===  'YearBuild' ||mp[0] === 'Mortgage' || mp[0] === 'Lease'){
                   if(mp[1]){
                    st = st + mp[1]
                } 
                }else if(mp[1] !== '' && mp[1] !== false && mp[1]){
                    st = st + `&${mp[0]}=${mp[1]}`
                }
            })
            // window.location.assign(`${'Tipic='+ tab+ ''}${st}#/search`);
            // let searchParams = new URLSearchParams(st);
            // console.log(searchParams)
            let newurl = window.location.protocol + "//" + window.location.host + '/' + '#/search?'+ st ;
            
            window.history.pushState({}, '', newurl)
             REALSTATEGETALLINIT(page, `${ st }${userid?"&_id=" + userid:""}` )
             if(e !== 'notScroll' ){
             setTimeout(() => {
                if(window.innerWidth < 800){
                    
                        window.scrollTo({
                            top:800,
                            left: 0,
                            behavior: "smooth"
                        })
                    }
                    }, 100);
                }
        }
            
        const setareahandller=(e)=>{
            
            if(e && e !== 'شهر' && e !== ""){
                const citid= cityall.find(er=>  er.name === e);
                setvalueHandller(e, "City");
               changefilehandller("", 'getallarea',`id=${citid.id}`)
            //    setarea('')
            // setvalueHandller("", "Area");
       }else{
        setvalueHandller("", "City");
        setvalueHandller("", "Area");
    }
        }

    const showAndSearchHandller =()=>{
        setShow(e=>!e);
        if(show){
            searchhandller('notScroll')
        }
        window.scrollTo(0,0)
    }
       
    
    return(
        <div className="srch-target">

        <Header tab={tab} settab={settab} auth={auth} sendreq={sendreq} ></Header>
        <CloseComponent>
        <section class="container-search">
        <section class="blog-post">
        <SearchRs   page={page} setpage={setpage}
                    REALSTATEGETALLINIT={REALSTATEGETALLINIT} 
                    length={length} filter={AllData} auth={auth}
                    addMarkinit={addMarkinit} lessmarkinit={lessmarkinit}
                    />
        </section>
        <button className="search-fix-position" onClick={showAndSearchHandller}>
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAGD0lEQVRoge2YbWxbVxnHf891nDqOnbVpkzRlare+bUxtk1KBgEHRKvgAFWIbVJtUKXKpECJQjZcPID4UJkCiQoihlSAqcE0RrGXZ1Gpr0IYYtBGsQiDSVouSLc2gb1m8ds4av82xz8MHx3fXsZ353rYsEv1/uuc553n5n3Puec554BZu4f8b4lVx586dLY2NjduBbUAXcAeweLZ7Cvi3iAwZY16wLOt4NBqdvv5wK+GaQE9Pz3qfz/dN4GEgWKdaWlWfAPbFYrFX3PqcD3UT2LFjR1MoFPoe8AjQ4NHfjKo+BuyNxWJZjzbKUBeBSCSyTkSeBjaUdYQ7sFZ0QftdEFyMNC0BQDMJSE9BfBRz+TRMT841ecrv9z944MCBiZtOYPfu3ZuNMc8BbbZwyUqsjfcj7XfV5USvjqNnj6JXzjnFFwuFwvZDhw6dcRlzGeYlMDvzf6UUvOXD6tqBrP4IiMvfRxUdH8Sc7gdTKEkvqur7Y7HYa+5DL8JXqyMSiQRE5HlgNQCNQax7v4Tc/j73wQOIIK2rsJatgYmzUJgBaLEs6761a9f+Znh4OO+FQM2fUUS+T/F4BMuH70NfgLb1FePag8Kq2yyWNlkE/UVZegauZgznrxkmU1qu0LYe+fAX0ZOPg8mjqltCodC3gO94IVB1KmePypdKBK3NDyNrPlo2JrwItnQ00NE8/2rE08o/JvJM58rlOnYCM/T7UjOpquu8bKWqKzB7zjcASOuq4p53YFmTsHWlD78lZPPKWMJwKakk3yrOdmiR8J6QsHaJRXtQ+PgdDQxeLHAl/fZqyJqtyH9OoYnzACER2Qv0uiVQMX2zGXaC2STl+9gjZVun0Sd8el0DDQLn3zT8c9KQK+hcM/bYLcstVrZYzBjl6Mt5jGOoxkcwJx8vNVMi0uk2Y1sVTovXg2KGDS+v2Pd5o7yeMoxcNbw4UagZPECuoLx4ucDoG4apKmlL2u+GcEep2ayqn3ITPFTfQttsBys2VXQahZMXChXymlAYmqw9Xjo3om8num3AkfqNV1kBSicP1J2orgfScbezWTlj74BqBO60v5pb3UfkEhIs87HarX41Ai228cBtHkJyiabFzpZrh9UIOFD7B71hUHNd6tUIXLNtZ968LuP1QLNlPlw7rEbgVfsrk3AfkUtIuszHuFv9CgIiMmQ3Jkc9BeUGZnLE2TztVr+CgDHmhdK3Xhqa233jMXHW/lTVP7lVryAQDAafBVIAmoyj8Zu3ChofcSaxVDAY/INbGxUE+vr6kqp62HZy5mluymmkip49ZjdF5Hd9fX1Jt2ZqHaP7gBkAnbqInhv0FON8MOdOlG6iADngh17sVH2RDQ0NvdHd3R0WkXuhuNSydDXSvMxTsHOhV8bQv//azgGq+qODBw/2e7E1XyLbC5wCwBTQU7+E11/24qMMGh9F//YLMPYLciwcDn/Xq72aBGKxWFZVHwAuAGguTWFwPzp2AtTLP6GYsb9gBvejubSz485UKvWQB4NAHWWVnp6eTT6f7zhwuy1s6SyWVTo31FZ0QOMj6Jmj6NSFWkMKIvL5aDR6qC6DDtRVXti1a1ebqvaLyNYy5VA7rNhUvBIHl9iFLTIJSCeKSeryGTQZn2tylOLN018SqFJIpLI/OPbkE64e93XXR/bs2bMomUx+G/gG0OzGiQM54MehUOjR6enp7SJyGPCrwqVEkulsjtZQ4KcDTx35ar0GXRd4IpHI8tkHeA/1E0kBvxWRfdFo1L7vRCKRB0EOX0ok/deyOTsgNyQ8l9d7e3tDmUxmO3Af0E3xIeQsr7+qqv+yLOvPgUBgoFaS+uQDDz2aSGf3Oo8FEaG1OfCTgacOf/2mEbiR+MRnPrd/Ojvz5TISwNJw4LHj/Ue+Np9uzdLi/xLjo8MD792wsS2XNx9wyjO5/Ae7NncvfmX4pedq6S4IAuCdxIIhAN5ILCgC4J7EOzzq3x388Vj/V1qaGn/uPGEUyObyn507dkESAHj+6JO94YD/ZyUSwcaG11oXddwzd9yC20JOjI8OD9yzsau9wWd1+t8Krn/mmV+l3u2YbuEWFhr+C9cMRyfEm5dDAAAAAElFTkSuQmCC"/>
        </button>
        {show&& <aside class=" filter-asid-res">
            <div className="filter-asid-fixed">
            {/* <div class="map type-asid">
                <h3 class="h3-asid">جستجو با محدوده</h3>
                <img class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, '')} src="img/map.png" alt="map"/>
            </div> */}

            <div class="type-tride type-asid">
                <h3 class="h3-asid">نوع معامله</h3>

                <div class="type-tride-btn">
                    {/* <button class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, '')}>همه</button>
                    <button>پیش فروش</button> */}
                    <button className={data.Tipic === 'rahn'?"button-tipic-active":"button-tipic"} 
                    onClick={()=>{
                    setvalueHandller('rahn', 'Tipic');
                }} >رهن و اجاره</button>
                    <button className={data.Tipic === 'sells'?"button-tipic-active":"button-tipic"} 
                    onClick={()=>{
                     setvalueHandller('sells', 'Tipic');
                     
                     }}>فروش</button>
                   {/*  <button>مشارکت در ساخت</button> */}
                </div>

            </div>
            <div class="type-home type-asid">
                <button className="search-button" onClick={searchhandller}>جستجو</button>
                </div>
            <div class="type-home type-asid">
                <select className="type-asid-field" onChange={(e)=>{
                    setvalueHandller(e.target.value, "TypeState")
                }} name="" id="">
               
                    <option value=''>نوع ملک</option>
                    <option >آپارتمان</option>
                    <option >ویلایی</option>
                    <option >باغ</option>
                    <option >تجاری</option>
                    <option > صنعتی</option>
                    <option > مزروعی</option>
                    
                </select>
            </div>
            <div class="type-home type-asid">
                
                {/* <input type="search"  className="type-asid-field" list="sellscity" 
                
                placeholder="شهر"
                name="sl1"
                 /> */}
                <select  id="sellscity" onChange={(e)=>setareahandller(e.target.value)}  >
                <option className="option" value="">همه</option>
            {cityall?cityall.map(mp=>(
                <option className='option'>
                    {mp.name}</option>
            )):""}
        </select>
                
            </div>
            <div class="type-price type-asid">
            {/* <input type="search" className="type-asid-field" list="sellsarea" 
                value={data.Area}
                name="sl2"
                placeholder="منطقه"
                onChange={(e)=>setvalueHandller(e.target.value,"Area")}
                disabled={areaall && data.City !== ''?false:true} />    */}
           <select id="sellsarea"  value={data.Area}
           onChange={(e)=>setvalueHandller(e.target.value,"Area")}
        //    disabled={areaall && data.City !== ''?false:true}
            disabled={areaall && data.City !== ''?false:true} 
            >
                <option className="option" value="">همه</option>
            {areaall?areaall?.map(mp=>(
                <option className='option' 
                >
                    {mp.areaName}</option>
            )):""}
            
</select>

            </div>
            {filter?<div  class="type-price type-asid">
                <select value={data.Mortgage} onChange={(e)=>
                    setvalueHandller(e.target.value , "Mortgage")
                } className="type-asid-field" name="" id="">
                     <option value=''> {data.Tipic === 'sells'?"قیمت":"رهن"}</option>
                    {filter.Price.map(mp=>(
                       <option value={mp.value} >
                            {mp.text}
                       </option>
                   ))}
                </select>
            </div>:null}
            { data.Tipic === 'rahn' && filter?.Lease?<div  class="type-price type-asid">
                <select className="type-asid-field" value={data.Lease} onChange={(e)=>
                    setvalueHandller(e.target.value , "Lease")
                } name="" id="">
                    <option value=''> اجاره</option>
                    {filter?.Lease.map(mp=>(
                       <option value={mp.value} >
                            {mp.text}
                       </option>
                   ))}
                </select>
            </div>:null}
            {filter?<div  class="type-price type-asid">
                <select className="type-asid-field" onChange={(e)=>
                    setvalueHandller(e.target.value , "Measure")
                } name="" id="">
                     <option value='' > متراژ</option>
                    {filter.Measure.map(mp=>(
                       <option value={mp.value} >
                            {mp.text}
                       </option>
                   ))}
                </select>
            </div>:null}
    

            <div class="type-room type-asid">
                <select className="type-asid-field" onChange={(e)=>
                    setvalueHandller(e.target.value, "SomeRoom")
               }>
                    <option value=''>تعداد خواب</option>
                    <option >  یک خواب</option>
                    <option >دو خواب</option>
                    <option >سه خواب</option>
                    <option > چهار خواب</option>
                </select>
            </div>

            {filter?<div class="type-age-bana type-asid">
                <select className="type-asid-field" onChange={(e)=>setvalueHandller(e.target.value, 'YearBuild')} name="" id="">
                <option value=''> سال ساخت</option>
                {filter.YearBuild.map(mp=>(
                    <option value={mp.value}>{mp.text}</option>
                ))}
            </select>
            </div>:null}

            <div class="type-emkanat type-asid">
                <h3 class="h3-asid">امکانات</h3>
                <div className="box-asid">
                    <label className="label-asid">پارکینگ</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Parking')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">آسانسور</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Assansor')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">سونا</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Sona')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">نگهبان</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Security')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">استخر</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Pool')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">پاسیو</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Pasio')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">انباری</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'OfStorage')} type="checkbox"/> 
                </div>

            </div>
            </div>
        </aside>}



        <aside class="filter-asid">
            <div className="filter-asid-fixed">
            {/* <div class="map type-asid">
                <h3 class="h3-asid">جستجو با محدوده</h3>
                <img class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, '')} src="img/map.png" alt="map"/>
            </div> */}

            <div class="type-tride type-asid">
                <h3 class="h3-asid">نوع معامله</h3>

                <div class="type-tride-btn">
                    {/* <button class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, '')}>همه</button>
                    <button>پیش فروش</button> */}
                    <button className={data.Tipic === 'rahn'?"button-tipic-active":"button-tipic"} 
                    onClick={()=>{
                    setvalueHandller('rahn', 'Tipic');
                }} >رهن و اجاره</button>
                    <button className={data.Tipic === 'sells'?"button-tipic-active":"button-tipic"} 
                    onClick={()=>{
                     setvalueHandller('sells', 'Tipic');
                     
                     }}>فروش</button>
                   {/*  <button>مشارکت در ساخت</button> */}
                </div>

            </div>
            <div class="type-home type-asid">
                <button className="search-button" onClick={searchhandller}>جستجو</button>
                </div>
            <div class="type-home type-asid">
                <select className="type-asid-field" onChange={(e)=>{
                    setvalueHandller(e.target.value, "TypeState")
                }} name="" id="">
               
                    <option value=''>نوع ملک</option>
                    <option >آپارتمان</option>
                    <option >ویلایی</option>
                    <option >باغ</option>
                    <option >تجاری</option>
                    <option > صنعتی</option>
                    <option > مزروعی</option>
                    
                </select>
            </div>
            <div class="type-home type-asid">
                
                {/* <input type="search"  className="type-asid-field" list="sellscity" 
                
                placeholder="شهر"
                name="sl1"
                 /> */}
                <select  id="sellscity" onChange={(e)=>setareahandller(e.target.value)}  >
                <option className="option" value="">همه</option>
            {cityall?cityall.map(mp=>(
                <option className='option'>
                    {mp.name}</option>
            )):""}
        </select>
                
            </div>
            <div class="type-price type-asid">
            {/* <input type="search" className="type-asid-field" list="sellsarea" 
                value={data.Area}
                name="sl2"
                placeholder="منطقه"
                onChange={(e)=>setvalueHandller(e.target.value,"Area")}
                disabled={areaall && data.City !== ''?false:true} />    */}
           <select id="sellsarea"  value={data.Area}
           onChange={(e)=>setvalueHandller(e.target.value,"Area")}
        //    disabled={areaall && data.City !== ''?false:true}
            disabled={areaall && data.City !== ''?false:true} 
            >
                <option className="option" value="">همه</option>
            {areaall?areaall?.map(mp=>(
                <option className='option' 
                >
                    {mp.areaName}</option>
            )):""}
            
</select>

            </div>
            {filter?<div  class="type-price type-asid">
                <select value={data.Mortgage} onChange={(e)=>
                    setvalueHandller(e.target.value , "Mortgage")
                } className="type-asid-field" name="" id="">
                     <option value=''> {data.Tipic === 'sells'?"قیمت":"رهن"}</option>
                    {filter.Price.map(mp=>(
                       <option value={mp.value} >
                            {mp.text}
                       </option>
                   ))}
                </select>
            </div>:null}
            { data.Tipic === 'rahn' && filter?.Lease?<div  class="type-price type-asid">
                <select className="type-asid-field" value={data.Lease} onChange={(e)=>
                    setvalueHandller(e.target.value , "Lease")
                } name="" id="">
                    <option value=''> اجاره</option>
                    {filter?.Lease.map(mp=>(
                       <option value={mp.value} >
                            {mp.text}
                       </option>
                   ))}
                </select>
            </div>:null}
            {filter?<div  class="type-price type-asid">
                <select className="type-asid-field" onChange={(e)=>
                    setvalueHandller(e.target.value , "Measure")
                } name="" id="">
                     <option value='' > متراژ</option>
                    {filter.Measure.map(mp=>(
                       <option value={mp.value} >
                            {mp.text}
                       </option>
                   ))}
                </select>
            </div>:null}
    

            <div class="type-room type-asid">
                <select className="type-asid-field" onChange={(e)=>
                    setvalueHandller(e.target.value, "SomeRoom")
               }>
                    <option value=''>تعداد خواب</option>
                    <option >  یک خواب</option>
                    <option >دو خواب</option>
                    <option >سه خواب</option>
                    <option > چهار خواب</option>
                </select>
            </div>

            {filter?<div class="type-age-bana type-asid">
                <select className="type-asid-field" onChange={(e)=>setvalueHandller(e.target.value, 'YearBuild')} name="" id="">
                <option value=''> سال ساخت</option>
                {filter.YearBuild.map(mp=>(
                    <option value={mp.value}>{mp.text}</option>
                ))}
            </select>
            </div>:null}

            <div class="type-emkanat type-asid">
                <h3 class="h3-asid">امکانات</h3>
                <div className="box-asid">
                    <label className="label-asid">پارکینگ</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Parking')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">آسانسور</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Assansor')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">سونا</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Sona')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">نگهبان</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Security')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">استخر</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Pool')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">پاسیو</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'Pasio')} type="checkbox"/> 
                </div>
                <div className="box-asid">
                    <label className="label-asid">انباری</label>
                        <input class="mt-10" onChange={(e)=>setvalueHandller(e.target.checked, 'OfStorage')} type="checkbox"/> 
                </div>

            </div>
            </div>
        </aside>
    </section>
    </CloseComponent>
    <Footer/>
    </div>
    )
}

const MapStateToProps=state=>{
    return{
        AllData:state.realstate.AllData,
        areaall: state.realstate.areaall,
        cityall: state.realstate.cityall,
        auth: state.auth.data,
        filter: state.request.filter,
        length: state.realstate.length,
        Tab: state.realstate.Tab,
    }
}
const MapDispatchToProps= dispatch=>{
    return{
        changefilehandller:(dt, path, id)=>dispatch(action.changefilehandller(dt, path, id)),
        sendreq:(data,authdt)=> dispatch(action.sendreq(data, authdt)),
        REALSTATEGETALLINIT:(page,query,limit)=>dispatch(action.REALSTATEGETALLINIT(page,query,limit)),
        getallfilterinit:(qu)=>dispatch(action.getallfilterinit(qu)),
        addMarkinit: (data)=> dispatch(action.addmarkinit(data)),
        lessmarkinit: (id)=> dispatch(action.lessmarkinit(id))
    }
}
export default  connect(MapStateToProps, MapDispatchToProps)(SearchResult)