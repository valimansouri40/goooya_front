import React, { useEffect, useState } from "react";
import '../MyMarks/MyMarks.css';
import * as action from '../../store/action/index';
import { connect } from "react-redux";
import Header from "../../components/Header/Header";
import { Link } from "react-router-dom";
import Paginate from "../../components/Paginate/Paginate";
import Spinner from "../../components/UI/spinner/Spinner";
import Footer from "../../components/Footer/Footer";
import { changeprice } from "../../components/UI/CardRealState/changePrice";
import CloseComponent from "../../components/CloseComponent/CloseComponent";
import { apidomainImage } from "../../store/utility/cookie";

/// بخش نشانک های من در نوبار صفحه سایت

const MyRealState=props=>{
        const {  auth,sendreq,myRealstateLength, myRealstate, getMyRealStateInit}= props;
        const [page, setpage]=useState(1)
        useEffect(()=>{
            getMyRealStateInit(page);
        },[])

        console.log(myRealstate)
    return(
        <div className='mymarks-target'>
            <Header auth={auth} sendreq={sendreq}></Header>
            <CloseComponent>
            <div className='mymarks-frame'>
             {myRealstate? myRealstate.length === 0?<div>موردی یافت نشد!!</div>:null:<Spinner/>} 
           {   myRealstate?myRealstate.map(mp=> <div className="mymarks-box"> 
             <div  className='mymarks-boximg'>
               
                
                        {mp.Image.length > 0 ? <img className="mymarks-img" src={apidomainImage +`${mp.Image[0]}`} />:null}

            </div>
            <div className='cardrealstate-boxdetail'>
               <div className='cardrealstate-boxonedetail' >
                    <img width='25px' height='25px' src="https://img.icons8.com/color/48/000000/measure.png"/>
                    <h3 className='cardrealstate-detail'>
                        {mp.Measure}
                    </h3>
               </div> 
               <div className='cardrealstate-boxonedetail' >
                    <img width='25px' height='25px' src="https://img.icons8.com/ios-filled/50/000000/bed.png"/>
                    <h3 className='cardrealstate-detail'>
                    {mp.SomeRoom}
                    </h3>
               </div>
                <div className='cardrealstate-boxonedetail' >
                    <img width='25px' height='25px' src="https://img.icons8.com/external-icongeek26-glyph-icongeek26/64/000000/external-property-due-diligence-icongeek26-glyph-icongeek26.png"/>
                    <h3 className='cardrealstate-detail'>  {mp.TypeState}</h3>
               </div>
               <div className='cardrealstate-boxonedetail' >
                    <h3 className='cardrealstate-detail'> قیمت  : </h3>
                    <h3 className='cardrealstate-detail'>  {mp.Mortgage && mp.Mortgage > 0 ?changeprice(mp.Mortgage):"توافقی"}  </h3> 
               
               </div>
               <div className='cardrealstate-boxonedetail' >
               <button className="card-btn"><Link style={{textDecoration:"none" ,color:"#fff"}} to={`/realstateupdate/${mp._id}`}> مشاهده صفحه </Link></button>
                </div>
            </div>
            </div>):null}</div> 
            <Paginate page={page} setpage={setpage} limit={8} length={myRealstateLength}></Paginate>
            </CloseComponent>
            <Footer/>
        </div>
    )
}

const MapStateToProps= state =>{

    return{
        
        myRealstate: state.realstate.myRealstate,
        myRealstateLength: state.realstate.myRealstateLength,
        auth: state.auth.data
    }
}

const MapDisPatchToProps= dispatch=>{

    return{
    
         sendreq:(data,authdt)=> dispatch(action.sendreq(data, authdt)),
         getMyRealStateInit : (page)=> dispatch( action.getMyRealStateInit(page))
    }
}

export default connect(MapStateToProps, MapDisPatchToProps)(MyRealState);
