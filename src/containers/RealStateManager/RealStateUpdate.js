import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router";
import Fields2 from "../../components/RealStateFieldsupdate/RealStateFieldupdate2";
import RealStateFieldsForlease from "../../components/RealStateFieldsupdate/RealStateFieldupdate1";
import * as action from '../../store/action/index';
import { ShowAlert } from "../../store/utility/alert";
import RealStateFieldupdate3 from "../../components/RealStateFieldsupdate/RealStateFieldupdate3";
import AdminPannelNav from "../../components/AdminPannelNav/AdminPannelNav";
import Spinner from "../../components/UI/spinner/Spinner";
import './RealStateManager.css';
import { Link } from "react-router-dom";
// صفحه آپدیت ملک در پنل مدیریت

const RealStateUpdate=(props)=>{
        const {OneData,loading, REALSTATEGETONEINIT, changefilehandller,
            REALSTATEPATCHINIT ,cityall, getadvisorinit ,advisor, role,areaall} = props;
            
        const paramid= useParams().id;
        
        useEffect(()=>{
            if(role){
                const rolelm = ['dealer', 'advisor'].includes(role.role)? role._id:undefined;
                REALSTATEGETONEINIT(paramid,rolelm)
        }
        },[REALSTATEGETONEINIT])
        const rolear= ['employee', 'admin']
      const [tab, settab]= useState('rahn');
      const [numpage, setnumpage]= useState(1);
      const [DataPostOne ,setDataPostOne]= useState();
      const [DataPosttwo ,setDataPosttwo]= useState();
      const [DataPostthree ,setDataPostthree]= useState();
      const [DataPost, setDataPost]=useState();
      useEffect(()=>{
        if(OneData){
            settab(OneData.Tab);
            setDataPostthree(OneData.AdvisorId)
        }
      },[OneData])
     

    let errorarr= {Tipic:"تب",
      City:"شهر",
      Area: "منطقه",
      TypeState:"نوع ملک",
      // YearBuild: "تعداد سال ساخت",
      Measure: "متراژ",
      [DataPost?.TheWay !== 'ازمالک'?"RepresenativePhoneNumber":"EsquierPhoneNumber"]:DataPost?.TheWay === 'ازمالک'?"شماره مالک":"شماره معرف",
      [DataPost?.TheWay !== 'ازمالک'?"RepresenativeName":"EsquierName"]:DataPost?.TheWay === 'ازمالک'?"نام مالک":"نام معرف",
      // Lease: "اجاره",
      // Mortgage:"رهن",
      // Location: "لوکیشن",
      AdvisorId: "مشاور املاک",
      // EsquierName: "نام ملک",
      // EsquierPhoneNumber: "شماره مالک"
  }
      if(tab !== "rahn"){
          errorarr= {Tipic:"تب",
      City:"شهر",
      Area: "منطقه",
      TypeState:"نوع ملک",
      // YearBuild: "تعداد سال ساخت",
      Measure: "متراژ",
      // Mortgage:"قیمت",
      // Location: "لوکیشن",
      AdvisorId: "مشاور املاک",
      [DataPost?.TheWay === 'ازمالک'?"RepresenativePhoneNumber":"EsquierPhoneNumber"]:DataPost?.TheWay === 'ازمالک'?"شماره مالک":"شماره معرف",
      [DataPost?.TheWay === 'ازمالک'?"RepresenativeName":"EsquierName"]:DataPost?.TheWay === 'ازمالک'?"نام مالک":"نام معرف",
      // EsquierName: "نام ملک",
      // EsquierPhoneNumber: "شماره مالک"
  }
      }

   
   
    useEffect(()=>{
            setDataPost({Tab:tab,...DataPostOne, ...DataPosttwo});
    },[DataPostOne,DataPosttwo, tab])
    useEffect(()=>{
        if(numpage === 3){
           
            getadvisorinit(DataPost.City, DataPost.Area); 
        }
    },[numpage])
    const SubmitDataHandller=async(dt)=>{
        // console.log(role.role)


        //  switch(role.role){
        //     case 'employee':
        //         DataPost.AdvisorId = DataPostthree
        //         break
        //     case 'admin':
        //         DataPost.AdvisorId = DataPostthree;
        //         break;
        //     case  'dealer':
        //         DataPost.AdvisorId = role._id;
        //         break;
        //     case 'advisor':
        //         DataPost.AdvisorId = role._id
        //         break
        //     default:delete DataPost.AdvisorId 
        // }
        // DataPost.RegistrarId = OneData.RegistrarId;
        DataPost.AdvisorId = DataPostthree;
        
       const er= Object.entries(errorarr);
       
       let errorhandller = []

       const entiresData= Object.entries(DataPost);

        entiresData.map((mp, index)=>{
            if(mp[1] === '' || mp[1] === undefined ){
              
                er.map(en=>{
                    
                    if(en[0] === mp[0]){
                        errorhandller.push(errorarr[mp[0]])
                    }
                  
                })}else if(mp[0] === 'Image'){
                    if(mp[1].length === 0 ){
                        errorhandller.push('عکس')
                    }
                }
        })
        
      if(errorhandller.length === 0){ 

        //   if(DataPost.Image[0].endsWith(".jpeg")){ 
        //       delete DataPost.Image
        //     };
        
            console.log(DataPost, "user")
          DataPost.RealStateNumber = OneData.RealStateNumber
          REALSTATEPATCHINIT(DataPost, paramid);
        }else{
            ShowAlert(errorhandller, 'را وارد نکردید', 'fail')
        }
    }
    let responsive = '' ;
            
    switch(numpage){
        case 1 : responsive= 'rstb-target-1';
        break;
        case 2 : responsive = 'rstb-target-2';
        break;
        default: responsive = 'rstb-target-3';
        break;
    }
    return(
        <section className={`rstb-target ${responsive}` }>
              <Link className="rstb-btn"
                to={'/'}>بازگشت به صفحه اصلی
               </Link>
                {/* <AdminPannelNav/> */}
{ OneData? <div className='builder'>
            
                <RealStateFieldsForlease
                OneData={OneData}
               role={role}
                tab={tab}
                cityall={cityall} areaall={areaall}
                changefilehandller={changefilehandller}
                numpage={numpage} setnumpage={setnumpage} 
                setDataPostOne={setDataPostOne}/>
                    <Fields2 numpage={numpage} 
                    DataPost ={DataPost}
                    OneData={OneData} loading={loading}
                     tab={tab}
                     role={role}
                    setnumpage={setnumpage}
                     setDataPosttwo={setDataPosttwo} SubmitDataHandller={SubmitDataHandller}/>
                     { role?rolear.includes(role.role)?<RealStateFieldupdate3 
                      role={role} loading={loading}
                      OneData={OneData}
                     tab={tab} getadvisorinit={getadvisorinit}
                     numpage={numpage}
                     advisor={advisor}
                    setnumpage={setnumpage}
                     setDataPostthree={setDataPostthree} SubmitDataHandller={SubmitDataHandller}
                     />:null:null}
            </div>:<Spinner/>}
        </section>
    )
}

const MapStateToProps=state=>{

    return{
        OneData: state.realstate.OneData,
        cityall: state.realstate.cityall,
        areaall: state.realstate.areaall,
        loading: state.realstate.loading,
        role: state.auth.data,
        advisor: state.auth.advisor
    }
}
const MapDispatchToProps=dispatch=>{
    return{
        REALSTATEGETONEINIT:(id,query)=>dispatch(action.REALSTATEGETONEINIT(id,query)),
        changefilehandller:(data,path, query)=>dispatch(action.changefilehandller(data,path, query)),
        REALSTATEPATCHINIT:(data, id)=>dispatch(action.REALSTATEPATCHINIT(data, id)),
        getadvisorinit : (cityid, area)=> dispatch(action.getadvisorinit(cityid, area) )
    }
}

export default connect(MapStateToProps, MapDispatchToProps)(RealStateUpdate)