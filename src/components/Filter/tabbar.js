import React, { useEffect, useRef, useState } from 'react';
import { ShowAlert } from '../../store/utility/alert';
import Modal from '../UI/Modal/Modal';




const Tabbar= (props)=>{
    const {worksampel,loading ,sendreq, auth , tab,getallwsinit}=props;
    const [numpgws, setnumpgws]=useState();
    const [tabs, settabs]=useState('');
    const [modal, setmodal] = useState(false);
    const [Name, setName]= useState('');
    const [PhoneNumber, setPhoneNumber]= useState()
    const switcharr=['mapdesign','Mapping','endofwork','Registrationwork','Advocacy', 'ExpertofJustice', 'endofwork', 'lisense','Executionandconstruction'];
    // const refselect = useRef();

    // useEffect(()=>{
    //     const rf = refselect.current;
    //     rf.innerHTML = 'در خواست گرفتن جواز'
    // },[])
    const changeTabs=(e)=>{
        settabs(e.target.value)
        // const rf = refselect.current;
        // rf.innerHTML = ' جواز'
        
    }
    useEffect(()=>{
        getallwsinit(1, 5, `Tab=${tabs}`);
    },[ tabs])
        
    // const setheihthandller =(i)=>{
    //     if(i !== numpgws){
    //     setnumpgws(i)
    // }else{
    //     setnumpgws();
    // }
    // }

    const submithandller=(e)=>{
            e.preventDefault()
       
            const data={
                FristName: !auth?Name:auth.FristName,
                PhoneNumber: !auth?PhoneNumber:auth.PhoneNumber,
                Type: tab,
                TypeWork:tabs
            }
            if(tabs !== ''  ){
                
                const pattern = new RegExp('^(\\0|0)?9\\d{9}$');
                // console.log(pattern.test(data.PhoneNumber), PhoneNumber, auth)
            if(pattern.test(data.PhoneNumber)){
            if(data.FristName !== '' ){
                
                // sendreq(data, auth?._id);
                setmodal(true);
                
            }else{
                setmodal(false);
            ShowAlert([], 'نام خودرا وارد نکرده اید', 'fail')
                
            }
        }else{
            setmodal(false);
            ShowAlert([], 'لطفا شماره تلفن همراه خود را وارد کنید', 'fail')
        }
    }else{
        tabs === '' && ShowAlert([], 'درخواست خود را انتخاب نکردید', 'fail');
        // data.FristName !=='' || data.PhoneNumber!=='' &&ShowAlert([], 'اطلاعات وارد نشده است', 'fail')
        // tabs !== '' &&data.FristName !=='' && data.PhoneNumber!=='' &&setmodal(true);

    }




// else{
//     console.log(e.target[0].value)
//     const data={
//         PhoneNumber: e.target[0].value,
//         Type: tab,
//         TypeWork:tabs
//     }
//     const pattern = new RegExp('^(\\0|0)?9\\d{9}$');
//             if(pattern.test(data.PhoneNumber)){
//         // sendreq(data, auth?._id);
//             }else{
//                 ShowAlert([], 'لطفا شماره تلفن همراه خود را وارد کنید', 'fail')
//             }
// }

    }
    const modalhandller= ()=>{
        setmodal(false);
        const data={
            FristName: !auth?Name:auth.FristName,
            PhoneNumber: !auth?PhoneNumber:auth.PhoneNumber,
            Type: tab,
            TypeWork:tabs
        }
         sendreq(data, auth?._id);
    }
    const arr={'mapdesign':'طراحی نقشه',
        'Mapping':'نقشه برداری',
        'Registrationwork':'کار های ثبتی',
        'Advocacy':'وکالت دادگاهی',
        'Executionandconstruction':' اجرا و ساخت',
        'ExpertofJustice':' کارشناس دادگستری',
        'endofwork':' پایان کار',
        'lisense':'جواز',
        }
    let mdl=null;
    if(modal){
        mdl =<Modal modal={modal} setmodal={setmodal}><div className='realstatemanager-modal' >
                <h3  className="realstatemanager-h3-box"> 
                {`اینجانب ${auth?auth.FristName + '' +auth.LastName:Name}
                 باشماره تماس ${auth?auth.PhoneNumber:PhoneNumber} درخواست انجام انجام ${arr[tabs]} دارم`}</h3>
                <div className="realstatemanager-modal-box">
                    <button className='realstatemanager-modal-box-btn-box' onClick={modalhandller}>ثبت </button>
                {/* <button className='realstatemanager-modal-box-btn-box red' >لغو</button> */}
                </div>
        </div></Modal>
    }

    useEffect(()=>{
        if(window.innerWidth > 600 && tabs === ''){
            settabs("mapdesign")
        }
    },[])
    return(
        
        <div class="engineering" >
            {mdl}
            <div
            class="align-items-start">
                <div  class="col-3 buttons">
                    <div style={{display:'flex', alignItems:'center',justifyContent:'center'
                    ,flexDirection:'column',width:"100%"}} class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                      <button class={tabs !== 'mapdesign'?"nav-link ":"nav-links-active"} onClick={changeTabs} value='mapdesign' >طراحی نقشه</button>
                        <button class={tabs !== 'Mapping'?"nav-link ":"nav-links-active"} onClick={changeTabs} value='Mapping' >نقشه برداری</button>
                        <button class={tabs !== 'Registrationwork'?"nav-link ":"nav-links-active"} onClick={changeTabs} value='Registrationwork' >کار های ثبتی</button>
                        <button class={tabs !== 'Advocacy'?"nav-link ":"nav-links-active"} onClick={changeTabs} value='Advocacy' >وکالت دادگاهی</button>
                        <button class={tabs !== 'Executionandconstruction'?"nav-link ":"nav-links-active"} onClick={changeTabs} value='Executionandconstruction' >اجرا و ساخت</button>
                        <button class={tabs !== 'ExpertofJustice'?"nav-link ":"nav-links-active"} onClick={changeTabs} value='ExpertofJustice' >کارشناس دادگستری</button>
                        <button class={tabs !== 'endofwork'?"nav-link ":"nav-links-active"} onClick={changeTabs} value='endofwork' >پایان کار</button>
                        <button class={tabs !== 'lisense'?"nav-link ":"nav-links-active"} onClick={changeTabs} value='lisense' >جواز</button>
                        {/* <button class={tabs !== 'contracting'?"nav-link ":"nav-links-active"} onClick={changeTabs} value='contracting' >پیمان کاری</button> */}
                    </div>
                </div>
                <div className='buttons-responsive'>
                    <select  onChange={changeTabs}  className="select-responsive" >
                    <option  class="nav-link3" onClick={changeTabs} value='' >انتخاب نوع درخواست</option>
                        <option  class="nav-link3" onClick={changeTabs} value='lisense' >جواز</option>
                    <option class="nav-link3" onClick={changeTabs} value='mapdesign' >
                        طراحی نقشه</option>
                        <option class="nav-link3" onClick={changeTabs} value='Mapping' >نقشه برداری</option>
                        <option class="nav-link3" onClick={changeTabs} value='Registrationwork' >کار های ثبتی</option>
                        <option class="nav-link3" onClick={changeTabs} value='Advocacy' >وکالت دادگاهی</option>
                        <option class="nav-link3" onClick={changeTabs} value='Executionandconstruction' >اجرا و ساخت</option>
                        <option class="nav-link3" onClick={changeTabs} value='ExpertofJustice' >کارشناس دادگستری</option>
                        <option class="nav-link3" onClick={changeTabs} value='endofwork' >پایان کار</option>
                    </select>
                </div>
                <div class="shows">
                <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane" id="v-pills-tarrahi">
                        <form onSubmit={submithandller} class="engener" >
                        <div  className='advisordata-phbox tabbar-whatsapp tab-whatsapp ' >
                            <p className='tabbar-whatsapp-text'>ارسال درخواست از طریق </p>
                            <a target="_blank" href={`https://wa.me/989209229205`}> 
                                <img width='50px' height='50px'
                                title='کلیک کنید' 
                                src="https://img.icons8.com/color/48/000000/whatsapp--v1.png"/>
                                </a>
                            </div> 
                                <span style={{marginRight:'1.2rem'}} className='request-name'> {tabs !== ''?`درخواست گرفتن ${arr[tabs]}`:''}</span>
                                <div style={{marginRight:'1.2rem'}} className="foormm">

                                        {!auth?<><input onChange={(e)=>setName(e.target.value)} id="namee" type="text" placeholder="نام و نام خانوادگی"/>
                                        </>:null}
                                        <input id="teel" 
                                        onChange={(e)=>setPhoneNumber(e.target.value)} 
                                        type="text" placeholder="شماره تماس"/>

                                    <input disabled={loading} style={{margin: '1rem 0'}} 
                                    type='submit' className='search-ok' value='درخواست' />
                                </div>
                            </form>
                            <div class="slider-box ">
                                <div class="header">
                                    <span className='header-tab-spn'>نمونه کارها</span>
                                    {worksampel?.length === 0?<p className='header-tab-p'>در مورد نقشه کشی از این تجهیزات استفاده میکنیم طی 1 هفته کار تحویل داده میشه و.....</p>
                                :null}
                                </div>
                                <div class="col-12 row klkl">
                                    {worksampel?worksampel.slice(0,3).map((mp,i)=><div class={`nemone-box 
//     ${i === numpgws?'more-text':'small-text'}`}

// onClick={()=>setheihthandller(i)}
>
                                        
                                           {!switcharr.includes(tabs)?<div  className='sampels'>
                                                <img className='sampels-img' src={`data:image/jpeg;base64,${mp.Image}`} alt=""/>
                                            {/* <h3 className='sampels-h3'>{mp.Text}</h3> */}
                                            <p className='paragraph'>
                                            {i === numpgws ? mp.Text :mp.Text.length > 30?mp.Text.slice(0,30)+  "...":mp.Text  }
                                                </p>
                                            </div>:
                                            <p className='paragraph'>{mp.Passage}</p>}
                                        
                                    </div>):null}
                                    {/* <div class="col-4 nemone-box">
                                        <a href="">
                                            <img src="https://exploreit.ir/wp-content/uploads/2022/03/sitemap-error-300x149.jpg" alt=""/>
                                            <h3>عنوان نمونه کار</h3>
                                        </a>
                                    </div>
                                    <div class="col-4 nemone-box">
                                        <a href="">
                                            <img src="https://exploreit.ir/wp-content/uploads/2022/03/sitemap-error-300x149.jpg" alt=""/>
                                            <h3>عنوان نمونه کار</h3>
                                        </a>
                                    </div> */}
                                </div>


                            </div>
                            
                        </div>
                    </div>
               </div>         
        </div>
    </div>
    
    )
}


export default Tabbar;

// {worksampel?worksampel.slice(0,3).map((mp,i)=>(
//     <div class={`col-4 nemone-box 
//     ${i === numpgws?'more-text':'small-text'}`}>
        
//            {!switcharr.includes(tabs)?<div className='sampels'>
//                 <img className='sampels-img' src={`data:image/jpeg;base64,${mp.Image}`} alt=""/>
//             {/* <h3 className='sampels-h3'>{mp.Text}</h3> */}
//             <p className='paragraph'onClick={()=>setnumpgws(1)}>
//                 {/* {i === numpgws ? mp.Text :mp.Text.length > 30?mp.Text.slice(0,30)+  "...":mp.Text  } */}
//                 </p>
//             </div>:
//             <p className='paragraph'>{mp.Passage}</p>}
        
//     </div>
//     )):null}