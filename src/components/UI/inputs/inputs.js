import React, { useEffect, useState } from "react";
import ok from '../../../assets/icons/icons8-circled-thin-32.png';
import circle from '../../../assets/icons/icons8-ok-32.png'
import { BeforeCityAndArea } from "../../../containers/AddAreaAndCity/Before";
import  './input.css';

const Inputs=(props)=>{
    const {showdatalist,changeAllVal, setshowdatalist,type,value,change, img,clickaddfied,id,valid,touch,eltype,setbol}= props;
    
    const [includeData, setincludeData] = useState([]);
    const path = window.location.hash
    let cssclass=["input"];
    if(!valid && touch){
        cssclass.push("notvalid");
        
    }
    const datalistHandller=async(e, idd)=>{
    //     const val = e.target.innerHTML
    //    console.log(idd, val)
        if(path === '#/addarea' && idd === "areaName" && value !== ''){
            
            setincludeData(await BeforeCityAndArea( 'getarea',`areaName=${value}`))
            setshowdatalist(false)
        }else if(path === '#/addcity'&& idd === "name"&& value !== ''){
            setincludeData(await BeforeCityAndArea( 'getcity',`cityName=${value}`))
    
            setshowdatalist(false)
        } if(path === '#/addarea'&& idd === "CityId"&& value !== ''){
            setincludeData(await BeforeCityAndArea( 'getcity',`cityName=${value}`))
    
            setshowdatalist(false)
        }
       }

       const setAndClose=(e,city)=>{
           if(city){changeAllVal(e.target.innerHTML, id);}
           setincludeData([])
       }
    let input;
    switch(type){
        case 'input':
           return input=<div>{img}<input {...eltype} value={value}
             onChange={change}  className={cssclass.join(' ')}/></div>;
            
        case 'texterea':
           return input= <textarea {...eltype} value={value}
             onChange={change} className={"textarea"}></textarea>;
             
        case 'select':
           return input=  <div className='selectbox'>
           <label className='label input-form'>   {eltype.placeholder}</label>
       <select className='select input-form'  onChange={change}  >
       <option className='option'></option>
   {eltype.options.map(mp=>(
       <option className='option'>
           {mp}</option>
   ))}
</select>
</div>
        case 'bol':
            return   input =  <div className={value?'boxtrue':'boxch'} onClick={props.Click}>
            <span className='txt'> {eltype.placeholder}</span>
            <img src={!value?ok:circle} className='imgchek'/>
        </div>
        case 'multiplieselect':
            return input = <div className='multiplebox'>
                   
                    {eltype.options.map(mp=>(
                            <label className="label-select-form">
                                <input type='checkbox' name='areaval' onChange={(e)=>clickaddfied(e,id, mp)} />
                                {mp}
                                </label>
                        ))}
                   
            </div>
        default:
            input=<div   className={"block"}>
                <input {...eltype} value={value}
                
             onClick={()=>{setbol(eltype.placeholder);
                 setshowdatalist(true);}}
             onChange={(e)=>{change(e,id);datalistHandller(e,id);}} className={cssclass.join(' ')}/>
                {img}
            
             </div>
    }

   
    return <div className={"pad"} >{input}
       { includeData.length > 0 && !showdatalist && value !== ''? 
               <div className=' inp-datalist'>
              <ul className='inp-datalist-ul'>
                
                        {includeData.map((mp, i)=>i <=22?<li  className='addareaandcity-datalist-span'
                            onClick={(e)=>setAndClose(e,mp.name)}
                        >
                                {mp.name?mp.name:mp.areaName}
                        </li>:null)}
                </ul> </div>:null}
    </div>;
}

export default Inputs;