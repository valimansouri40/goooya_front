import React from "react";
import './DataBoxRes.css';


const DescriptionRes= (props)=>{
        const {OneData}= props

            // const tsinclude=['آپارتمان','تجاری'];
    return(
        <div className="dbr-target">
              
                <div className="dbr-des-target">
                <div className="dbr-des-box">
                            <div className="gallery-labelbox">
                                <h3 className="gallery-label">
                                    توضیحات
                                </h3>
                                <span className="gallery-border"></span>
                            </div>
                            <div className="dbr-des-div">
                                    <p className="dbr-des-p">
                                        {OneData.Explain}
                                    </p>
                            </div>
                    </div>
                </div>
                
        </div>
    )
}


export default DescriptionRes; 