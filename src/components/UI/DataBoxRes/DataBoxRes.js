import React from "react";
import { calender } from "../../../store/utility/utils";
import './DataBoxRes.css';


const DataBoxRes= (props)=>{
        const {OneData}= props

            const tsinclude=['آپارتمان','تجاری'];
    return(
        <div className="dbr-target">
                
                <div className="dbr-des-target">
                <div className="dbr-des-box">
                            <div className="gallery-labelbox">
                                <h3 className="gallery-label">
                                    اطلاعات ملک
                                </h3>
                                <span className="gallery-border"></span>
                            </div>
                            <div className="dbr-datail-div">
                            <div className='databoxps-block'>
                             <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'>شهر:</span> 
                            <span className='databoxps-spn'> {OneData.City} </span>
                            
                             </h2>
                    </div>
                    <div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> منطقه :</span> 
                            <span className='databoxps-spn'> {OneData.Area} </span>
                            
                             </h2>
                    </div>
                    {OneData.Mortgage && OneData.Mortgage > 0 && !OneData.AgreedPrice ? <div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'>{OneData.Tab === 'sells'? 'قیمت':"رهن"} : </span> 
                            <span className='databoxps-spn'>   {(OneData.Mortgage * 1).toLocaleString()} </span>
                            
                             </h2>
                          
                    </div>:<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'>{OneData.Tab === 'sells'? 'قیمت':"رهن"} : </span> 
                            <span className='databoxps-spn'>   توافقی </span>
                            
                             </h2>
                          
                    </div>}
                    {OneData.Lease && OneData.Lease > 0?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'>اجاره : </span> 
                            <span className='databoxps-spn'>   {(OneData.Lease * 1).toLocaleString()} </span>
                            
                             </h2>
                    </div>:null}
                    {OneData.PricePerMeter && OneData.PricePerMeter > 0 && OneData.Tab === 'sells'?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'>قیمت به متر : </span> 
                            <span className='databoxps-spn'>   {(OneData.PricePerMeter * 1).toLocaleString()} </span>
                            
                             </h2>
                    </div>:null}
                    {OneData.SomeRoom?<div className='databoxps-block'>
                           
                             <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'>تعداد اتاق خواب:</span> 
                            <span className='databoxps-spn'> {OneData.SomeRoom} </span>
                           
                             </h2>
                    </div>:null}
                    <div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> نوع ملک :</span> 
                            <span className='databoxps-spn'> {OneData.TypeState} </span>
                             </h2>
                    </div>
                    {tsinclude.includes(OneData.TypeState) && OneData.Floors ? <div className='databoxps-block'>
                             <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> طبقه :</span> 
                            <span className='databoxps-spn'> {OneData.Floors} </span>
                             </h2>
                    </div>:null}
                    {OneData.Kitchen?<div className='databoxps-block'>
                             <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'>آشپز خانه:</span>  
                            <span className='databoxps-spn'> {OneData.Kitchen} </span>
                             </h2>
                    </div>:null}
                    {OneData.DocumentOnership?<div className='databoxps-block'>
                             <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'> وضعیت مالکیت :</span> 
                            <span className='databoxps-spn'> {OneData.DocumentOnership} </span>
                            
                             </h2>
                    </div>:null}
                    {OneData.DocumentSituation?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> وضعیت سند :</span> 
                            <span className='databoxps-spn'> {OneData.DocumentSituation} </span>
                            
                             </h2>
                    </div>:null}

                    {OneData.PropertyDirection?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> جهت نما :</span> 
                            <span className='databoxps-spn'>
                                     {OneData.PropertyDirection}
                                      </span>
                            
                             </h2>
                    </div>:null}
                    {OneData.Service?<div className='databoxps-block'>
                             <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'> سرویس :</span> 
                            <span className='databoxps-spn'>{OneData.Service} </span>
                            
                             </h2>
                    </div>:null}
                    { OneData.DocumentSituation?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> وضعیت سند :</span> 
                            <span className='databoxps-spn'> {OneData.DocumentSituation} </span>
                            
                             </h2>
                            
                    </div>:null}
                    { OneData.DocumentType?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> نوع سند :</span> 
                            <span className='databoxps-spn'> {OneData.DocumentType} </span>
                            
                             </h2>
                            
                    </div>:null}
                    { OneData.TrasMeasure && OneData.Tras?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> متراژ بالکن :</span> 
                            <span className='databoxps-spn'> {OneData.TrasMeasure} </span>
                            
                             </h2>
                            
                    </div>:null}
                    {OneData.MonthlyCharge && OneData.MonthlyCharge > 0?<div className='databoxps-block'>         
                    <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'>شارژ ماهیانه : </span> 
                            <span className='databoxps-spn'> {(OneData.MonthlyCharge* 1).toLocaleString()}  </span>
                           
                             </h2>
                    </div>:null}
                    {OneData.HowManyFloors && OneData.HowManyFloors?<div className='databoxps-block'>         
                    <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'>تعداد طبقات : </span> 
                            <span className='databoxps-spn'> {OneData.HowManyFloors}  </span>
                           
                             </h2>
                    </div>:null}
                    { OneData.TrasMeasure && OneData.Tras?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> متراژ بالکن :</span> 
                            <span className='databoxps-spn'> {OneData.TrasMeasure} </span>
                            
                             </h2>
                        </div>:null}
                    {OneData.HowManyUnits && OneData.HowManyUnits >0?<div className='databoxps-block'>         
                    <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'>تعداد واحد : </span> 
                            <span className='databoxps-spn'> {OneData.HowManyUnits}  </span>
                           
                             </h2>
                    </div>:null}
                    {OneData.HeaterSystem?<div className='databoxps-block'>
                             <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'>سیستم گرمایشی :</span> 
                            <span className='databoxps-spn'> {OneData.HeaterSystem} </span>
                             </h2>
                    </div>:null}
                    {OneData.PropertySituation?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> وضعیت اسکان ملک :</span> 
                            <span className='databoxps-spn'> {OneData.PropertySituation} </span>
                             </h2>
                    </div>:null}
                    {OneData.CoolerSystem?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> سیستم سرمایشی :</span>
                             <span className='databoxps-spn'>{OneData.CoolerSystem} </span>
                             </h2>
                            
                    </div>:null}
                    <div className="databoxps-block-id">
                   <h2 className='databoxsp-h2'>
                           <span className="databoxps-spn"> تاریخ ثبت آگهی :</span>  
                           <span className="databoxps-spn">{calender(OneData.createAt)}</span>
                             </h2>
                  
                    </div>
                    <div className="databoxps-block-id">
                   <h2 className='databoxsp-h2'>
                           <span className="databoxps-spn">آیدی ملک :</span>  
                           <span className="databoxps-spn">{OneData.RealStateNumber}</span>
                             </h2>
                  
                    </div>
                            </div>
                            <div className="gallery-labelbox">
                                <h3 className="gallery-label">
                                   جزئیات
                                </h3>
                                <span className="gallery-border"></span>
                            </div>
                        <div className="dbr-box-detail2">
                        {OneData.OfStorage? <div className="dbr-box-detail-field-big">
                        <h2 className='databoxsp-h2'>
                               انباری 
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                           
                             {OneData.Parking ?<div className="dbr-box-detail-field-big">
                           <h2 className='databoxsp-h2'>
                                   پارکینگ 
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                             {OneData.Assansor? <div className="dbr-box-detail-field-big">
                                    <h2 className='databoxsp-h2'>
                                    آسانسور
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}

                             {OneData.Tras? <div className="dbr-box-detail-field-big">
                                   <h2 className='databoxsp-h2'>
                                   تراس
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                             {OneData.Security?<div className="dbr-box-detail-field-big">
                            <h2 className='databoxsp-h2'>
                                   نگهبان
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                             {OneData.Pool?<div className="dbr-box-detail-field-big">
                            <h2 className='databoxsp-h2'>
                                  استخر
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}

                             {OneData.Gas?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    گاز 
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                             {OneData.Wastewater? <div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    فاضلاب
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                             {OneData.endOfWork? <div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    پایان کار
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {OneData.Janitor?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                   سرایداری
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {OneData.License?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    جواز
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {OneData.Phone?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    تلفن
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {OneData.DbWindow?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    پنجره دوجداره
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {OneData.TheWell?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    چاه
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             

                             {OneData.Pasio?<div className="dbr-box-detail-field-big">
                            <h2 className='databoxsp-h2'>
                                 پاسیو
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {OneData.Sona?<div className="dbr-box-detail-field-big">
                            <h2 className='databoxsp-h2'>
                                   سونا
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {OneData.Jacuzzi?<div className="dbr-box-detail-field-big">
                            <h2 className='databoxsp-h2'>
                                   جکوزی
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                        </div>
                    </div>
                </div>
                
                
        </div>
    )
}


export default DataBoxRes; 