import React from "react";
// import './DataBoxDetail.css';



const DataBoxdep= (props)=>{
        const {dt}= props;


        return(
            <div className='databoxps-box'>
                   
                    {dt.PropertyDirection?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> جهت نما  : </span> 
                            <span className='databoxps-spn'>
                                     {dt.PropertyDirection}
                                      </span>
                            
                             </h2>
                    </div>:null}
                    {dt.Service?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'> سرویس  : </span> 
                            <span className='databoxps-spn'> {dt.Service} </span>
                            
                             </h2>
                    </div>:null}
                    {dt.PropertySituation?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> وضعیت اسکان ملک  : </span> 
                            <span className='databoxps-spn'> {dt.PropertySituation} </span>
                            
                             </h2>
                    </div>:null}
                    {dt.HeaterSystem?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'> سیستم گرمایشی  : </span> 
                            <span className='databoxps-spn'> {dt.HeaterSystem} </span>
                           
                             </h2>
                    </div>:null}
                    {dt.CoolerSystem?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> سیستم سرمایشی  : </span> 
                            <span className='databoxps-spn'> {dt.CoolerSystem} </span>
                             </h2>
                            
                    </div>:null}
                    {dt.Entry?<div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                    <span className='databoxps-spn'> ورود از : </span> 
                            <span className='databoxps-spn'> {dt.Entry} </span>
                             </h2>
                        
                    </div>:null}
                    {dt.Kitchen? <div className='databoxps-block'>
                    <h2 className='databoxsp-h2'>
                             <span className='databoxps-spn'>آشپز خانه : </span>  
                            <span className='databoxps-spn'>  {dt.Kitchen} </span>
                             </h2>
                    </div>:null}
                   
                    <div className="databoxps-block">
                   <h2 className='databoxsp-h2'>
                              آیدی ملک : {dt.RealStateNumber}
                             </h2>
                  
                    </div>
                   {dt.OfStorage? <div className="dbr-box-detail-field-big">
                        <h2 className='databoxsp-h2'>
                               انباری 
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                           
                             {dt.Parking ?<div className="dbr-box-detail-field-big">
                           <h2 className='databoxsp-h2'>
                                   پارکینگ 
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                             {dt.Assansor? <div className="dbr-box-detail-field-big">
                                    <h2 className='databoxsp-h2'>
                                    آسانسور
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}

                             {dt.Tras? <div className="dbr-box-detail-field-big">
                                   <h2 className='databoxsp-h2'>
                                   تراس
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                             {dt.Security?<div className="dbr-box-detail-field-big">
                            <h2 className='databoxsp-h2'>
                                   نگهبان
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                             {dt.Pool?<div className="dbr-box-detail-field-big">
                            <h2 className='databoxsp-h2'>
                                  استخر
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}

                             {dt.Gas?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    گاز 
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                             {dt.Wastewater? <div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    فاضلاب
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             
                             </div>:null}
                             {dt.endOfWork? <div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    پایان کار
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {dt.Janitor?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                   سرایداری
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {dt.License?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    جواز
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {dt.Phone?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    تلفن
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {dt.DbWindow?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    پنجره دوجداره
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {dt.TheWell?<div className="dbr-box-detail-field-big">
                                 <h2 className='databoxsp-h2'>
                                    چاه
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             

                             {dt.Pasio?<div className="dbr-box-detail-field-big">
                            <h2 className='databoxsp-h2'>
                                 پاسیو
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {dt.Sona?<div className="dbr-box-detail-field-big">
                            <h2 className='databoxsp-h2'>
                                   سونا
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}
                             {dt.Jacuzzi?<div className="dbr-box-detail-field-big">
                            <h2 className='databoxsp-h2'>
                                   جکوزی
                             </h2>
                             <img className="dbr-box-detail-img" 
                             src="https://img.icons8.com/fluency/48/000000/ok.png"/>
                             </div>:null}


            </div>
        )

}

export default DataBoxdep;


{/* <div className="databoxps-block">
{ dt.Jacuzzi?<h2 className='databoxsp-h2'>
                     جکوزی دارد
          </h2>:null}
        { dt.Tras ?<h2 className='databoxsp-h2'>
                تراس دارد
          </h2>:null}
 </div>
 <div className="databoxps-block">
{ dt.Sona?<h2 className='databoxsp-h2'>
            سونا دارد
          </h2>:null}
        { dt.Pasio ?<h2 className='databoxsp-h2'>
                پاسیو دارد
          </h2>:null}
 </div>
 <div>
{ dt.Labi?<h2 className='databoxsp-h2'>
             لابی دارد
          </h2>:null}
        { dt.Pool ?<h2 className='databoxsp-h2'>
                استخر دارد
          </h2>:null}
 </div>
 <div className="databoxps-block">
{ dt.Security?<h2 className='databoxsp-h2'>
             نگهبان دارد
          </h2>:null}

 </div> */}