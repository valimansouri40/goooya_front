import React, { useState } from "react";
import './DeleteBox.css';
import searchIcon from '../../../assets/icons/icons8-search-50.png'
import deleteIcon from '../../../assets/icons/icons8-trash-can-64.png'

const DeleteBox=(props)=>{
    const {data,  search,setsearch,includeData, findhandller, deleterealstatepost}=props;
        const [showdatalist, setshowdatalist ]= useState(false);
        let array;
        if(data){
            array =data.length >0? Object.entries(data[0]):null
        }

        const keyPressHandller=(e)=>{
                if(e.charCode === 13){
                    findhandller()
                }
        }

        const setAndClose=(e)=>{
            setsearch(e.target.innerHTML);
            // setshowdatalist(true);
            // console.log(search, e)
          
                
                findhandller(e.target.innerHTML)
            
        }
    return(
        <div className='boxdt' onClick={()=>setshowdatalist(true)}>
           <div className='findbox'>
               <input type='text' value={search} placeholder={window.location.hash !=="#/addcity"?"نام منطقه را وارد کنید":"نام شهر را وارد کنید"} 
               className='inp' onKeyPress={keyPressHandller} 
               onChange={(e)=>{setsearch(e.target.value);setshowdatalist(false)}} />
               <img src={searchIcon}  onClick={()=>findhandller()} className='find' />
              { includeData.length > 0 && !showdatalist && search !== ''?
              <div className='addareaandcity-datalist-box'>
              <ul className='addareaandcity-datalist-box-ul'>
                
                        {includeData.map((mp, i)=>i <=22?<li className='addareaandcity-datalist-span'
                        onClick={setAndClose}>
                                {window.location.hash === '#/addcity'?mp.name:mp.areaName}
                        </li>:null)}
                </ul> </div>:null}
               </div> 
               
            <div className='showdata'>
                {data?data.length >0?<img src={deleteIcon}
                title="حذف شهر" className='delete' onClick={deleterealstatepost}/>:null:null}
                {data?data.length >0?array.map((mp,index)=>(
                   <> 
                   <p className='showsome' 
                   style={{padding:'1rem'}}>{mp[0] + ' : ' + mp[1]} </p>

                    </>
                )):'!!موردی یافت نشد':null}
            </div>
        </div>
    )
}
export default DeleteBox;
