import React, { useEffect, useState } from "react";
import { ShowAlert } from "../../store/utility/alert";
import Map from '../UI/GoogleMaps/GoogleMaps';
import Select from "../UI/Select/Select";
import './RealStateFields.css';


const RealStateFieldupdate1=(props)=>{
        const {setnumpage,role ,numpage,OneData , setDataPostOne, changefilehandller,
             cityall,tab, areaall}=props;
        // console.log(OneData)
        const [city, setcity]=useState(OneData.City);
        const [cityid, setcityid]=useState('');
        const [erea, seterea]= useState(OneData.Area);
        const [allerea, setallerea]= useState([]);
        // const [areadetail, setareadetail]=useState(OneData.AreaDetails);
        const [typeAdrress, settypeAdrress]=useState(OneData.Type);
        const [typestate, settypestate]= useState(OneData.TypeState);
        const [years, setyears]= useState(OneData.YearBuild);
        const [measure, setmesure]= useState(OneData.Measure);
        const [lease, setlease]= useState(OneData.Lease);
        const [mortgage, setmortgage]= useState(OneData.Mortgage === 0?'':OneData.Mortgage);
        const [immediat, setimmediet]= useState(OneData.Immediatly);
        const [fullmortgage, setfullmortgage]= useState(OneData.FullMortgage);
        const [aggrement, setagrement]= useState(OneData.Aggrement);
        const [location, setlocation]=useState(OneData.Location);
        const [cityandareaid, setcityandareaid]= useState();
        const [esquirename, setesquiername]= useState(OneData.EsquierName)
        const [esquireph, setesquierph]= useState(OneData.EsquierPhoneNumber)
        const [esquirephtest, setesquierphts]= useState('');
        const [areatype, setAreatype]= useState(OneData.AreaObjId); 
        const [represenativeName, setRepresenativeName] = useState(OneData.RepresenativeName)
        const [represenativePhoneNumber, setRepresenativePhoneNumber] = useState(OneData.RepresenativePhoneNumber)
        const [PricePerMeter, setPricePerMeter] = useState(OneData.PricePerMeter);
        const [theWay, setTheWay] = useState(OneData.TheWay);
        const [Exchange, setExchange] = useState(false);
        const [AgreedPrice, setAgreedPrice] =useState(false)

        const rolear2= ['admin','employee', 'advisor','dealer']

        const rolear= ['admin']
        useEffect(()=>{
            changefilehandller(null, 'getallcity','')
               
        },[])
        useEffect(()=>{
            if(role){ if(!rolear2.includes(role.role)){
                 setesquiername(role.FristName + ' ' + `${role.LastName?role.LastName:''}`);
                 setesquierph(role.PhoneNumber)
             }}
        },[ role])

         const submitHandller=()=>{
            
            if(!theWay && theWay === ''){
                ShowAlert([],'طریقه دریافت ملک را وارد کنید','fail')
                return;
            }
           
            const data=tab === 'rahn'?{
                Tipic:tab,
                City:city,
                Area: erea,
                Type:typeAdrress,
                TypeState:typestate,
                YearBuild: years,
                Measure: measure,
                Lease: lease,
                Mortgage:mortgage,
                Immediatly: immediat,
                Aggrement: aggrement,
                Location: location,
                cityandareaid: cityandareaid ,
                EsquierName: esquirename,
                EsquierPhoneNumber: esquirephtest,
                AreaObjId: areatype,
                RepresenativePhoneNumber:represenativePhoneNumber,
                RepresenativeName: represenativeName,
                TheWay: theWay,
                Exchange:Exchange,
                AgreedPrice:AgreedPrice
            }:{
                Tipic:tab,
                City:city,
                Area: erea,
                Type:typeAdrress,
                TypeState:typestate,
                YearBuild: years,
                Measure: measure,
                Mortgage:mortgage,
                Immediatly: immediat,
                FullMortgage: fullmortgage,
                Aggrement: aggrement,
                Location: location,
                cityandareaid: cityandareaid ,
                EsquierName: esquirename,
                EsquierPhoneNumber: esquirephtest,
                AreaObjId: areatype,
                RepresenativePhoneNumber:represenativePhoneNumber,
                RepresenativeName: represenativeName,
                PricePerMeter: typeof PricePerMeter === "string"?Number(PricePerMeter.replace(/,/g,'')):PricePerMeter,
                TheWay: theWay,
                Exchange:Exchange,
                AgreedPrice:AgreedPrice
            }
           
            setDataPostOne(data)
            setnumpage(2)
           
        }
        useEffect(()=>{
            setallerea(areaall)
        },[areaall])

        const setvaluehandller= (e)=>{
            if(role){
                if(e && e !== 'شهر' && rolear.includes(role.role)){
             const citid= cityall.find(er=>  er.name === e);
             setcity(e)

            changefilehandller(null, 'getallarea',`id=${citid.id}`)
            
    }else{
        seterea('');
        setAreatype('');
        setcity('');
        setcityid('');

    }}      
        }

      const setareahandller=(e)=>{
          if(e && e !== 'منطقه'){
          
          const oneeria= areaall.find(rs=> rs.areaName === e);
         
          seterea(oneeria.areaName);
        //   setareadetail(oneeria)
        }else{
                seterea();
                setAreatype('')
          }
      }
      const setesquierphhandller=(e)=>{
        
        setesquierph(e);
      
    }
    useEffect(()=>{
        if(new RegExp('^(\\0|0)?9\\d{9}$').test(esquireph)){
            setesquierphts(esquireph);
        }else{
            setesquierphts();
        }
    },[esquireph])
     
    const addcommaToNumber =(e, setval)=>{
        const pattern = ['1','2','3','4','5','6','7','8','9','0'];
        // console.log(e.replace(/(\d{3})/g, ",$1"))
        
         let addcomma ='';
            for(let i = 0; i<= e.length - 1; i++){
                   
                    for(let j = 0; j<= pattern.length - 1; j++) {
                    if(e[i] === pattern[j]){
                       addcomma =  addcomma + e[i]
                    }
                }
                    
            }
         
             addcomma = addcomma.split( /(?=(?:\d{3})+(?:\.|$))/g ).join( "," );
            //  console.log(addcomma.split( /(?=(?:\d{3})+(?:\.|$))/g ))
            setval( addcomma);
    
      }
      
      useEffect(()=>{
        if(mortgage !== ''){
            setAgreedPrice(false)
        }
      },[mortgage])
    return(
        <div className={numpage === 1?'leaseform': 'hidden'} >
                
                <div className='formbox'>
                    <div className='selectbox'>
                    <label className='label'>   شهر</label>
                <select  className='select responesive-select'   onChange={(e)=>setvaluehandller(e.target.value)}  >
                <option className='option'  >
                       شهر
                    </option>
                    {cityid === '' ?<option selected>{OneData.City}
                     </option>:null}
                    {role?cityall && rolear.includes(role.role)?cityall.map(mp=>(
                <option className='option'>
                    {mp.name}</option>
            )):null:null}
            {role?!rolear.includes(role.role) && role.CitysAndAreas.length > 0?role.City.map((mp, i)=>(
                cityid === '' && mp.name === OneData.City? null:
                <option value={i} className='option'>
                {mp.name}</option>
            )):null:null}
        </select>
        </div>
        
        {role?rolear.includes(role.role) ?  <div  className='selectbox'>
                <label className='label'> منطقه</label>
           <select  className='select responesive-select' value={erea}  
           onChange={(e)=>setareahandller(e.target.value)}  disabled={ city !== ''?false:true} >
                    <option className='option'  >
                        منطقه
                    </option>
            {allerea?allerea.map(mp=>(
                <option className='option' 
                >
                    {mp.areaName}</option>
            )):<option className='option' 
            >
                {erea}</option>}
</select>
 </div>:null:null}
{ role?!rolear.includes(role.role) && role.CitysAndAreas.length > 0?
 <div className='selectbox'>
                <label className='label'> منطقه</label>
           <select  value={erea} className='select responesive-select'  
           onChange={(e)=>setareahandller(e.target.value)}  disabled={ city !== ''?false:true} >
                    <option className='option'  >
                        منطقه
                    </option>
                    {cityid === '' ?<option selected>{OneData.Area}
                     </option>:null}
             {role.CitysAndAreas.map(mp=>(
                mp.objid._id === cityid ?<option className='option'>
                    {mp.areaName}</option>:null
            ))}
</select>
 </div>:null:null}
   
  
 <Select selectRes={true} val={typestate} setvaluehandller={settypestate}
                            array={['نوع ملک','آپارتمان','ویلایی','تجاری','صنعتی','باغ','مزروعی']}
                            >نوع ملک</Select>

                <div className='inpcls'><label className='label'>متراژ</label>
                 <input type='number' value={measure} className='inp inp-res inp-one' onChange={(e)=>setmesure(e.target.value)} /></div>
                <div className='inpcls'><label className='label'>
                     سال بنا</label> <input type='number' min='1' className='inp inp-res inp-one'
                    value={years}  onChange={(e)=>setyears(e.target.value)} /></div>
               <div className='inpcls'><label className='label'> 
               {tab === 'rahn'? 'رهن':'قیمت'}</label> <input type='number' value={mortgage} className='inp inp-res inp-one'
                 onChange={(e)=>setmortgage(e.target.value)} /></div>
               {tab === 'rahn'?<div className='inpcls'><label className='label'> اجاره</label> 
               <input type='number' value={lease} className='inp inp-res inp-one' onChange={(e)=>setlease(e.target.value)} /></div>:
               <div className='inpcls'><label className='label'> قیمت به متر</label> 
               <input type='text' value={PricePerMeter} className='inp inp-res inp-one' 
               onChange={(e)=>addcommaToNumber(e.target.value, setPricePerMeter)} /></div>}

               <div className='inpcls'><label className='label'> آدرس</label> 
               <input type='text' className="inp inp-res inp-one" value={typeAdrress} onChange={(e)=>settypeAdrress(e.target.value)}  /></div>
               {role?rolear2.includes(role.role)?<><div className='inpcls'><label className='label'> شماره مالک</label> 
               <input type='tel' className="inp inp-res inp-one" value={esquireph} onChange={(e)=>setesquierphhandller(e.target.value)}  /></div>
               <div className='inpcls'><label className='label'> نام مالک</label> 
               <input type='text' className="inp inp-res inp-one" value={esquirename} onChange={(e)=>setesquiername(e.target.value)}  /></div></>:null:null}
               <div className='inpcls'><label className='label'> شماره معرف</label> 
               <input type='tel' className='inp inp-res inp-one' name='tell' value={represenativePhoneNumber}
                onChange={(e)=>setRepresenativePhoneNumber(e.target.value)}  /></div>
               <div className='inpcls'><label className='label'> نام معرف</label> 
               <input type='text' className='inp inp-res inp-one' name='represnativeName'
               value={represenativeName}
                onChange={(e)=>setRepresenativeName(e.target.value)}  /></div>
               <div className="btn-box">
               {tab === 'rahn'?
               
               <button className={fullmortgage?'btnui-ok':'btnui'} onClick={()=>setfullmortgage(e=>!e)} >رهن کامل</button>:null}
               {/* <Button val={aggrement} setvalue={()=>setagrement(true)} >قیمت توافقی</Button> */}
                {/* <Button val={immediat} setvalue={setimmediet} >فوری</Button> */}
               <button className={immediat?'btnui-ok ':'btnui '} onClick={()=>setimmediet(e=>!e)}>فوری</button>
               <button style={{width:'80%'}} className={aggrement?'btnui-ok special':'btnui special'} onClick={()=>setagrement(e=>!e)}>پیشنهاد ویژه سایت</button>
               
                </div>
                <div className="btn-box">
               <button className={Exchange?'btnui-ok special':'btnui special'} onClick={()=>setExchange(e=>!e)}>معاوضه</button>
               <button className={AgreedPrice?'btnui-ok special':'btnui special'} disabled={mortgage === ''  ?true:false} onClick={()=>{setAgreedPrice(e=>!e);setmortgage('');}}>قیمت توافقی</button>
                   
               </div>
                <Select  selectRes={true} val={theWay} setvaluehandller={setTheWay}
                            array={['',' ازمالک',' از مشاور یا بنگاه ']}
                            >طریقه دریافت ملک </Select>
                <button className='send1' onClick={submitHandller}>صفحه بعد</button>
                </div>
                <div  className='rstf-googelmapbox'>
                      <Map location={location} setlocation={setlocation}></Map>
                </div>
        </div>
    )
}


export default RealStateFieldupdate1;