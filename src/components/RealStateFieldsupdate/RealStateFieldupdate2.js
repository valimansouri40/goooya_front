
import React, { useEffect, useState } from 'react';
import CheckBox from '../UI/CheckBox/CheckBox';
import InputField from '../UI/InputField/InputField';
import Select from '../UI/Select/Select';
import  './RealStateFields.css'
import imgicon from '../../assets/icons/icons8-picture-64.png';
import { ShowAlert } from '../../store/utility/alert';
import { apidomainImage } from '../../store/utility/cookie';
import Trash from '../../assets/icons/icons8-trash-can-64.png';

const RealStateFieldupdate2= (props)=>{
    const {setDataPosttwo,numpage,loading, setnumpage,role, SubmitDataHandller,OneData}=props;
    // const mpimg= OneData.Image.map(m=>{ return 'data:image/jpeg;base64,' + m})
    // console.log(OneData)
    const [sub, setsub]=useState(OneData.Subject)
    const [explain, setexplain]=useState(OneData.Explain)
    const [masahat, setmasahat]=useState(OneData.Masahat)
    const [floors , setfloors ]=useState(OneData.Floors)
    const [image, setimage]=useState(OneData.Image)
    const [someroom, setsomerom]=useState(OneData.SomeRoom)
    const [propertyDirection, setpropertyDirection]=useState(OneData.PropertyDirection)
    const [balcony, setbalcony]=useState(OneData.Balcony)
    const [flooring, setflooring]=useState(OneData.Flooring)
    const [kitchen, setkitchen]=useState(OneData.Kitchen)
    const [service, setservice]=useState(OneData.Service)
    const [parking, setparking]=useState(OneData.Parking)
    const [ofstorage, setofstorage]=useState(OneData.OfStorage)
    const [sona, setsona]=useState(OneData.Sona)
    const [tras, settras]=useState(OneData.Tras)
    const [security, setsecurity]=useState(OneData.Security)
    const [assansor, setassansor]=useState(OneData.Assansor)
    const [coolerSystem, setcoolerSystem]=useState(OneData.CoolerSystem)
    const [heaterSystem, setheaterSystem]=useState(OneData.HeaterSystem)
    const [propertySituation, setpropertySituation]=useState(OneData.PropertySituation)
    const [documentSituation, setdocumentSituation]=useState(OneData.DocumentSituation)
    const [documentOnership, setdocumentOnership]=useState(OneData.DocumentOnership)
    const [entry, setentry]=useState(OneData.Entry)
    const [pasio, setpasio]=useState(OneData.Pasio)
    const [pool, setpool]=useState(OneData.Pool)
    const [Jacuzzi, setJacuzzi]=useState(OneData.Jacuzzi)
    const [labi, setlabi]=useState(OneData.Labi)
    const [conferencehall, setconferencehall]=useState(OneData.ConferaenceHall)
    const [TheWell, setTheWell]= useState(OneData.TheWell);
    const [DbWindow, setDbwindows] = useState(OneData.DbWindow);
    const [Phone, setPhone] = useState(OneData.Phone);
    const [License, setLicense] = useState(OneData.License);
    const [Gas, setGas] = useState(OneData.Gas);
    const [Wastewater, setWastewater] = useState(OneData.Wastewater);
    const [endOfWork, setEndOfWork]= useState(OneData.endOfWork);
    const [Janitor, setJanitor] = useState(OneData.Janitor);
    const [HowManyFloors, setHowManyFloors] = useState(OneData.HowManyFloors);
    const [HowManyUnits, setHowManyUnits] = useState(OneData.HowManyUnits);
    const [MonthlyCharge, setMonthlyCharge] = useState(String(OneData.MonthlyCharge));
    const [TrasMeasure, setTrasMeasure] = useState(OneData.TrasMeasure);
    const [DocumentType, setDoucmentType]= useState(OneData.DocumentType);
    const [GoooyaExplain, setGoooyaExplain]= useState(OneData.GoooyaExplain);
    const [ListImagesDeleted , setList] = useState([]);
    const limitrole= ['employee', 'admin'];
   const [num,  setnum] = useState(1);
    const [numimg, setnumimg]=useState(0);

    const getBase64 = (file) => new Promise(function (resolve, reject) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result)
        reader.onerror = (error) => reject('Error: ', error);
    })

    const convertObjToStr = (image)=>{
        let encods=[];
        if(image.length > 0 ){
            // const entires= Object.entries(image);
           image.map(file=>typeof file === "object"?getBase64(file).then(result=>encods.push(result)):encods.push(file))
            // for(let i= 0 ;  i > image.length ; i++){
                //     encods.append('Image', image[i])
                // }
                
        }
        return encods;
    }

    
    const submitHandller= async()=>{
      
            const datatwo={
                Subject:sub,
                Explain:explain,
                Masahat:masahat,
                Floors:floors,
                Image: convertObjToStr(image),
                SomeRoom:someroom,
                PropertyDirection:propertyDirection,
                Balcony: balcony,
                Flooring:flooring, 
                Kitchen:kitchen,
                Service:service,
                Parking:parking,
                OfStorage:ofstorage,
                Sona:sona,
                Tras:tras,
                Security:security,
                Assansor:assansor,
                CoolerSystem:coolerSystem,
                HeaterSystem:heaterSystem,
                PropertySituation:propertySituation,
                DocumentSituation:documentSituation,
                DocumentOnership: documentOnership,
                Entry:entry,
                Pasio:pasio,
                Pool:pool,
                Jacuzzi:Jacuzzi,
                Labi:labi,
                ConferaenceHall:conferencehall,
                ListImagesDeleted:   ListImagesDeleted ,
                TheWell:TheWell,
                DbWindow:DbWindow,
                Phone:Phone,
                License:License,
                Gas:Gas,
                Wastewater:Gas,
                endOfWork:endOfWork,
                Janitor:Janitor,
                HowManyFloors:HowManyFloors,
                HowManyUnits:HowManyUnits,
                MonthlyCharge:typeof MonthlyCharge === 'string'? Number(MonthlyCharge.replace(/,/g,'')) : MonthlyCharge,
                TrasMeasure:TrasMeasure,
                DocumentType: DocumentType,
                GoooyaExplain:GoooyaExplain
            }
            
        setDataPosttwo(datatwo);

        if(!limitrole.includes(role.role)){
            if(num === 1) {
                setnum(2)
                
                    ShowAlert([], 'لظفا یک بار دیگر کلیک کنید','success')
               
                
                return;
            };
            SubmitDataHandller();
            setnum(1);
        }else{
            setnumpage(3);
        }
    }
    // console.log(image[0].endsWith(".jpeg")?[]:OneData.Image)
    

    const sendimage=(e)=>{
        const imageObj = {...e};
      
        // for(let i in imageObj){
        //     console.log(imageObj[i], i)
        //     if(Number(i) > 9){
        //         delete imageObj[i]
        //     }
        // }
       
        let imageArrlength = Object.entries(imageObj)
        
        let sizeimg = 0;
        // console.log(imageArrlength.length)

        imageArrlength = imageArrlength.map(el => el[1]).concat(image)
        for(let i = 0; i < imageArrlength.length ; i++){
                if(imageArrlength[i].size){
                 sizeimg = imageArrlength[i].size + sizeimg;
                    }
        }
        // console.log(imageObj)
        // console.log(imageArrlength)
        // if(imageArrlength )
        if(sizeimg < 50000000){
          
            
           
           if(imageArrlength.length > 10){
            imageArrlength = imageArrlength.splice(0, 10)
            
        }
        
           setimage(imageArrlength);
            
        setnumimg(0)
        }else{
            ShowAlert([],'حجم عکس بیشتر از 50 مگابایت است','fail')
        }
        
    }
  
    const nextMyImage=()=>{

        if( numimg < image.length - 1 ){
                setnumimg(num=> num + 1)
        }else{
            setnumimg(0)
        }
        
    }
   
    const lastMyImage=()=>{

        if( 0 < numimg ){
                setnumimg(num=> num - 1)
        }else{
            setnumimg(image.length - 1)
        }
    }
    const addcommaToNumber =(e, setval)=>{
        const pattern = ['1','2','3','4','5','6','7','8','9','0'];
        // console.log(e.replace(/(\d{3})/g, ",$1"))
        
         let addcomma ='';
            for(let i = 0; i<= e.length - 1; i++){
                   
                    for(let j = 0; j<= pattern.length - 1; j++) {
                    if(e[i] === pattern[j]){
                       addcomma =  addcomma + e[i]
                    }
                }       
            }
             addcomma = addcomma.split( /(?=(?:\d{3})+(?:\.|$))/g ).join( "," );
            //  console.log(addcomma.split( /(?=(?:\d{3})+(?:\.|$))/g ))
            setval( addcomma);
      }
      const removeImgHandller=()=>{
          if(image.length > 0 &&typeof image[0] === 'string'){

              setList( [image[numimg]].concat(ListImagesDeleted))
          }
        const allImage = image.filter((el, i) => i !== numimg );
        
      console.log(image)

        setimage(allImage)
        
        
            setnumimg(0)
        
        
      // console.log(image)
    }
    // console.log(ListImagesDeleted)
    return(
        <div  className={numpage === 2?'fields':'hidden'} >
            <div className='rstb-field' style={{marginTop:"4rem"}}>
            <InputField val={sub} setval={setsub} >عنوان</InputField>
            <div className='selectbox'>
                    <label className='label'>   توضیحات</label>
                    <textarea value={explain} className='inputfield-textarea' 
                    onChange={e=>setexplain(e.target.value)}></textarea>
                    </div>
            <InputField val={masahat} setval={setmasahat} >مساحت (متر مبع)</InputField>
            {/* <InputField val={floors} setval={setfloors} >تعداد طبقات</InputField> */}
            <div className='inpcls'><label className='label'> مبلغ شارژ</label> 
               <input type='text' value={MonthlyCharge} className='inp inp-res' 
               onChange={(e)=>addcommaToNumber(e.target.value, setMonthlyCharge)} /></div>
            <div className='setimgbox'>
                    <label className='label' for='img'>  <span className='labeltxt'> افزودن تصویر</span>
                    <img src={imgicon} className='iconimg' />
                     </label>
                   
                    <input type='file'accept="image/png, image/jpeg" multiple={true}
                     style={{display:'none'}} id='img' name='img'
                    //  onClick={()=>{
                    //     if(image.length > 0 && image[0].endsWith('.jpeg')){
                    //         setimage([])
                    //     }
                    //  }}
                     onChange={(e)=>sendimage(e.target.files)} />
                     {image.length > 0?<div  className='imgtarget'>
                         {typeof image[numimg] !== 'object'?<img src={apidomainImage + image[numimg]} width='100%' height='100%'  className='inputimg'/>:
                         <img src={URL.createObjectURL(image[numimg])} width='100%' height='100%'  className='inputimg'/>}
                         <span className='delete-img' onClick={removeImgHandller}>
                           <img src={Trash} 
                                  width='40px' height="40px" /> 
                           </span>
                         {image.length > 1?<><span className='changenum-2' onClick={lastMyImage}></span>
                        <span className='changenum-1' onClick={nextMyImage}></span></>:null}
                     </div>:null}
                    </div>
            </div>
            <div className='rstb-field'>

                
            <InputField  val={HowManyFloors} setval={setHowManyFloors} >طبقه چندم</InputField>
            <InputField  val={HowManyUnits} setval={setHowManyUnits} >چند واحدی</InputField>
            <InputField  val={floors} setval={setfloors} >تعداد طبقات</InputField>
            <div className='selectbox'>
                <label className='label'>   جواز و انشعابات</label>
            </div>
            
            <CheckBox val={DbWindow} changeval={setDbwindows} >پنجره دوجداره</CheckBox>
            <CheckBox val={TheWell} changeval={setTheWell} >چاه</CheckBox>
            <CheckBox val={Gas} changeval={setGas} >گاز</CheckBox>
            <CheckBox val={Phone} changeval={setPhone} >تلفن</CheckBox>
            <CheckBox val={Wastewater} changeval={setWastewater} >فاضلاب</CheckBox>
            <CheckBox val={License} changeval={setLicense} >جواز</CheckBox>
            <CheckBox val={endOfWork} changeval={setEndOfWork} >پایان کار</CheckBox>
           
            
                </div>
            <div className='rstb-field'>
            <Select  val={someroom} array={['','یک خواب','دو خواب','سه خواب','چهار خواب']} setvaluehandller={setsomerom}>تعداد اتاق خواب</Select>
            <Select  val={propertyDirection} array={['','شرقی','غربی','جنوبی','شمالی']} setvaluehandller={setpropertyDirection}>جهت نما</Select>
            <Select  val={balcony} array={['',"آجر","سنگ","سایر"]} setvaluehandller={setbalcony}>نوع نما</Select>
            <Select  val={kitchen} array={['',"چوبی","ممبران","ام دی اف","بدون کابینت","سایر"]} setvaluehandller={setkitchen}>آشپز خانه</Select>
            
            <CheckBox val={ofstorage} changeval={setofstorage} >انباری</CheckBox>
            <CheckBox val={sona} changeval={setsona} >سونا</CheckBox>
            <CheckBox val={parking} changeval={setparking} >پارکینگ</CheckBox>
            
           
            <CheckBox val={security} changeval={setsecurity} >نگهبان</CheckBox>
            <CheckBox val={assansor} changeval={setassansor} >آسانسور</CheckBox>
            <CheckBox val={Janitor} changeval={setJanitor} >سرایداری</CheckBox>

            </div>
            <div className='rstb-field'>
            <Select   val={DocumentType} array={['','دفترچه ای','تک برگ']} setvaluehandller={setDoucmentType}>نوع سند</Select>

            <CheckBox val={tras} changeval={settras} >بالکن</CheckBox>
            <InputField  val={TrasMeasure} dis={!tras} setval={setTrasMeasure} >متراژ بالکن</InputField>
            <Select  val={service} array={['',"ایرانی","فرنگی","ایرانی و فرنگی"]} setvaluehandller={setservice}>سرویس</Select>
            <CheckBox val={pool} changeval={setpool} >استخر</CheckBox>
            <CheckBox val={pasio} changeval={setpasio} >پاسیو</CheckBox>
            <CheckBox val={Jacuzzi} changeval={setJacuzzi} >جکوزی</CheckBox>
            <CheckBox val={labi} changeval={setlabi} >لابی</CheckBox>
            <CheckBox val={conferencehall} changeval={setconferencehall} >سالن اجتماعات</CheckBox>
            <div className='selectbox'>
                    <label className='label'>   توضیحات برای گویا</label>
                    <textarea value={GoooyaExplain} className='inputfield-textarea' 
                    onChange={e=>setGoooyaExplain(e.target.value)}></textarea>
                    </div>
            </div>
            <div className='rstb-field'>
            <Select  val={coolerSystem} array={["","کولر","هواساز","فاقد"]} setvaluehandller={setcoolerSystem}> سیستم سرمایشی </Select>
            <Select  val={heaterSystem} array={["","شوفاژ","هواساز","پکیج","بخاری"]} setvaluehandller={setheaterSystem}> سیستم گرمایشی </Select>
            <Select  val={propertySituation} array={["","تخلیه","در دست مالک","در دست مستاجر"]} setvaluehandller={setpropertySituation}> وضعیت اسکان ملک </Select>
            <Select  val={documentSituation} array={["","شخصی","تعاونی","اوقافی","زمین شهری","قولنامه ای"]} setvaluehandller={setdocumentSituation}> وضعیت سند </Select>
            <Select  val={documentOnership} array={["","شش دانگ","مشاعی"]} setvaluehandller={setdocumentOnership}> مالکیت سند </Select>
            <Select  val={entry} array={["","از حیاط","از خیابان","از محوطه"]} setvaluehandller={setentry}> ورودی ملک </Select>
            <Select  val={flooring} array={['','پارکت','سرامیک','موزاییک']} setvaluehandller={setflooring}>کفپوش</Select>
            
           
            <div className='btn-2-box'>
            <button className='send' onClick={()=>setnumpage(1)}>بازگشت</button>
            {role?limitrole.includes(role.role)?<button className='send' 
            onClick={submitHandller}>صفحه بعد</button>:null:null}
            {role?!limitrole.includes(role.role)?<button className='send2' onClick={submitHandller}>
                {!loading?"ارسال":<>درحال ارسال<span className="spin">
            </span></>}</button>:null:null}
            </div>
            </div>

        </div>
    )
}

export default RealStateFieldupdate2;


 