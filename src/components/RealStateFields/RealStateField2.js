import React, { useState } from 'react';
import CheckBox from '../UI/CheckBox/CheckBox';
import InputField from '../UI/InputField/InputField';
import Select from '../UI/Select/Select';
import  './RealStateFields.css'
import imgicon from '../../assets/icons/icons8-picture-64.png';
import { ShowAlert } from '../../store/utility/alert';
import Trash from '../../assets/icons/icons8-trash-can-64.png';

const Fields2= (props)=>{
    const {setDataPosttwo,numpage,loading,
         setnumpage,role, SubmitDataHandller}=props;

    const [sub, setsub]=useState('')
    const [explain, setexplain]=useState('')
    const [masahat, setmasahat]=useState('')
    const [floors , setfloors ]=useState('')
    const [image, setimage]=useState([])
    const [someroom, setsomerom]=useState('')
    const [propertyDirection, setpropertyDirection]=useState('')
    const [balcony, setbalcony]=useState('')
    const [flooring, setflooring]=useState('')
    const [kitchen, setkitchen]=useState('')
    const [service, setservice]=useState('')
    const [parking, setparking]=useState(false)
    const [ofstorage, setofstorage]=useState(false)
    const [sona, setsona]=useState(false)
    const [tras, settras]=useState(false)
    const [security, setsecurity]=useState(false)
    const [assansor, setassansor]=useState(false)
    const [coolerSystem, setcoolerSystem]=useState('')
    const [heaterSystem, setheaterSystem]=useState('')
    const [propertySituation, setpropertySituation]=useState('')
    const [documentSituation, setdocumentSituation]=useState('')
    const [documentOnership, setdocumentOnership]=useState('')
    const [entry, setentry]=useState('')
    const [pasio, setpasio]=useState(false)
    const [pool, setpool]=useState(false)
    const [Jacuzzi, setJacuzzi]=useState(false)
    const [labi, setlabi]=useState(false)
    const [conferencehall, setconferencehall]=useState(false)
    const [TheWell, setTheWell]= useState(false);
    const [DbWindow, setDbwindows] = useState(false);
    const [Phone, setPhone] = useState(false);
    const [License, setLicense] = useState(false);
    const [Gas, setGas] = useState(false);
    const [Wastewater, setWastewater] = useState(false);
    const [endOfWork, setEndOfWork]= useState(false);
    const [Janitor, setJanitor] = useState(false);
    const [HowManyFloors, setHowManyFloors] = useState();
    const [HowManyUnits, setHowManyUnits] = useState();
    const [MonthlyCharge, setMonthlyCharge] = useState('');
    const [TrasMeasure, setTrasMeasure] = useState('');
    const [DocumentType, setDoucmentType]= useState('');
    const [GoooyaExplain, setGoooyaExplain]= useState('');
    
    const limitrole= ['employee', 'admin'];
    const [numimg, setnumimg]=useState(0);

    const getBase64 = (file) => new Promise(function (resolve, reject) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result)
        reader.onerror = (error) => reject('Error: ', error);
    })

    
    const submitHandller= ()=>{

        let encods=[];

        if(image.length > 0){
            // const entires= Object.entries(image);
             image.map(file=>getBase64(file).then(result=>encods.push(result)))
                // for(let i= 0 ;  i > image.length ; i++){
                //     encods.append('Image', image[i])
                // }
        }else{
            ShowAlert([], 'عکس را وارد نکردید','fail')
            return;
        }
       
            const datatwo={
                Subject:sub,
                Explain:explain,
                Masahat:masahat,
                Floors:floors,
                Image: encods,
                SomeRoom:someroom,
                PropertyDirection:propertyDirection,
                Balcony: balcony,
                Flooring:flooring, 
                Kitchen:kitchen,
                Service:service,
                Parking:parking,
                OfStorage:ofstorage,
                Sona:sona,
                Tras:tras,
                Security:security,
                Assansor:assansor,
                CoolerSystem:coolerSystem,
                HeaterSystem:heaterSystem,
                PropertySituation:propertySituation,
                DocumentSituation:documentSituation,
                DocumentOnership: documentOnership,
                Entry:entry,
                Pasio:pasio,
                Pool:pool,
                Jacuzzi:Jacuzzi,
                Labi:labi,
                ConferaenceHall:conferencehall,
                TheWell:TheWell,
                DbWindow:DbWindow,
                Phone:Phone,
                License:License,
                Gas:Gas,
                Wastewater:Gas,
                endOfWork:endOfWork,
                Janitor:Janitor,
                HowManyFloors:HowManyFloors,
                HowManyUnits:HowManyUnits,
                MonthlyCharge:Number(MonthlyCharge.replace(/,/g,'')),
                TrasMeasure:TrasMeasure,
                DocumentType: DocumentType,
                GoooyaExplain:GoooyaExplain
            }
        setDataPosttwo(datatwo);

        if(!limitrole.includes(role.role)){
           
        SubmitDataHandller()
    }else{
        setnumpage(3);
        window.scrollTo(0,0);

    }
    }

    const sendimage=(e)=>{
        const imageObj = {...e};

        // for(let i in imageObj){
        //     console.log(imageObj[i], i)
        //     if(Number(i) > 9){
        //         delete imageObj[i]
        //     }
        // }
        let imageArrlength = Object.entries(imageObj)
        console.log(image?Object.entries(imageObj):null)
        let sizeimg = 0;
        console.log(imageArrlength.length)

        imageArrlength = imageArrlength.map(el => el[1]).concat(image)
        for(let i = 0; i < imageArrlength.length ; i++){
                 sizeimg = imageArrlength[i].size + sizeimg;

        }
        // console.log(imageObj)
        if(imageArrlength )
        if(sizeimg < 50000000){
          
            
           console.log(imageArrlength)
           if(imageArrlength.length > 10){
            imageArrlength = imageArrlength.splice(0, 10)
            console.log(imageArrlength)
           
        }
            setimage(imageArrlength);
            
        setnumimg(0)
        }else{
            ShowAlert([],'حجم عکس بیشتر از 50 مگابایت است','fail')
        }
        
    }
    const nextMyImage=()=>{

        if( numimg < image.length - 1 ){
                setnumimg(num=> num + 1)
        }else{
            setnumimg(0)
        }
        
    }
    
    const lastMyImage=()=>{

        if( 0 < numimg ){
                setnumimg(num=> num - 1)
        }else{
            setnumimg(image.length - 1)
        }
        
    }
    let innerWidth = window.innerWidth;
    document.addEventListener('resize',()=>{
        innerWidth = window.innerWidth;
    })
    
    const addcommaToNumber =(e, setval)=>{
        const pattern = ['1','2','3','4','5','6','7','8','9','0'];
        // console.log(e.replace(/(\d{3})/g, ",$1"))
        
         let addcomma ='';
            for(let i = 0; i<= e.length - 1; i++){
                   
                    for(let j = 0; j<= pattern.length - 1; j++) {
                    if(e[i] === pattern[j]){
                       addcomma =  addcomma + e[i]
                    }
                }       
            }
             addcomma = addcomma.split( /(?=(?:\d{3})+(?:\.|$))/g ).join( "," );
            //  console.log(addcomma.split( /(?=(?:\d{3})+(?:\.|$))/g ))
            setval( addcomma);
      }
      
      const removeImgHandller=()=>{
          const allImage = image.filter((el, i) => i !== numimg );
        console.log(image)

          
          setimage(allImage)
          if(numimg === image.length){
              setnumimg(e=> e - 1)
          }
        // console.log(image)
      }
    return(
        <div className={numpage === 2?'fields':'hidden'} >
            
            <div className='rstb-field'>
            <InputField  val={sub} setval={setsub} >عنوان</InputField>
            <div className='selectbox'>
                    <label className='label'>   توضیحات</label>
                    <textarea value={explain} className='inputfield-textarea' 
                    onChange={e=>setexplain(e.target.value)}></textarea>
                    </div>
            <InputField  val={masahat} setval={setmasahat} >مساحت (متر مبع)</InputField>
            {/* <InputField  val={floors} setval={setfloors} >تعداد طبقات</InputField> */}
            <div className='inpcls'><label className='label'> مبلغ شارژ</label> 
               <input type='text' value={MonthlyCharge} className='inp inp-res' 
               onChange={(e)=>addcommaToNumber(e.target.value, setMonthlyCharge)} /></div>
            <div className='setimgbox'>
                    <label className='uploadimage-label' for='img'>  <span className='labeltxt'> *افزودن تصویر</span>
                    <img src={imgicon} className='iconimg' />
                     </label>
                   
                    <input type='file' accept="image/png, image/jpeg" 
                    multiple={true} style={{display:'none'}} id='img' name='img'
                     onChange={(e)=>sendimage(e.target.files)} />
                     {image && image.length > 0?<div 
                     className='imgtarget'>
                         <img src={URL.createObjectURL(image[numimg])}  width='100%' height='100%'
                           className='inputimg'/>
                           <span className='delete-img' onClick={removeImgHandller}>
                           <img src={Trash} 
                                  width='40px' height="40px" /> 
                           </span>
                         {image.length > 1?<><span className='changenum-2' onClick={lastMyImage}></span>
                        <span className='changenum-1' onClick={nextMyImage}></span></>:null}
                     </div>:
                     <img src={imgicon}  width='100%' height='100%'
                     className='inputimg'/>
                     }
                    </div>
                    
            </div>
            
            <div className='rstb-field'>

            <InputField  val={HowManyFloors} setval={setHowManyFloors} >طبقه چندم</InputField>
            <InputField  val={HowManyUnits} setval={setHowManyUnits} >چند واحدی</InputField>
            <InputField  val={floors} setval={setfloors} >تعداد طبقات</InputField>
            <div className='selectbox'>
                <label className='label'>   جواز و انشعابات</label>
            </div>
            
            <CheckBox val={DbWindow} changeval={setDbwindows} >پنجره دوجداره</CheckBox>
            <CheckBox val={TheWell} changeval={setTheWell} >چاه</CheckBox>
            <CheckBox val={Gas} changeval={setGas} >گاز</CheckBox>
            <CheckBox val={Phone} changeval={setPhone} >تلفن</CheckBox>
            <CheckBox val={Wastewater} changeval={setWastewater} >فاضلاب</CheckBox>
            <CheckBox val={License} changeval={setLicense} >جواز</CheckBox>
            <CheckBox val={endOfWork} changeval={setEndOfWork} >پایان کار</CheckBox>
           
            


            </div>
            <div className='rstb-field'>
            <Select selectRes={innerWidth < 1250?true:false} val={someroom} array={['','یک خواب','دو خواب','سه خواب','چهار خواب']} setvaluehandller={setsomerom}>تعداد اتاق خواب</Select>
            <Select selectRes={innerWidth < 1250?true:false} val={propertyDirection} array={['','شرقی','غربی','جنوبی','شمالی']} setvaluehandller={setpropertyDirection}>جهت نما</Select>
            <Select selectRes={innerWidth < 1250?true:false} val={balcony} array={['',"آجر","سنگ","سایر"]} setvaluehandller={setbalcony}>نوع نما</Select>
            <Select selectRes={innerWidth < 1250?true:false} val={kitchen} array={['',"چوبی","ممبران","ام دی اف","بدون کابینت","سایر"]} setvaluehandller={setkitchen}>آشپز خانه</Select>
            
            <CheckBox val={ofstorage} changeval={setofstorage} >انباری</CheckBox>
            <CheckBox val={sona} changeval={setsona} >سونا</CheckBox>
            <CheckBox val={parking} changeval={setparking} >پارکینگ</CheckBox>
            
           
            <CheckBox val={security} changeval={setsecurity} >نگهبان</CheckBox>
            <CheckBox val={assansor} changeval={setassansor} >آسانسور</CheckBox>
            <CheckBox val={Janitor} changeval={setJanitor} >سرایداری</CheckBox>

            </div>
            <div className='rstb-field'>
            <Select selectRes={innerWidth < 1250?true:false} val={DocumentType} array={['','دفترچه ای','تک برگ']} setvaluehandller={setDoucmentType}>نوع سند</Select>

            <CheckBox val={tras} changeval={settras} >بالکن</CheckBox>
            <InputField  val={TrasMeasure} dis={!tras} setval={setTrasMeasure} >متراژ بالکن</InputField>
            <Select selectRes={innerWidth < 1250?true:false} val={service} array={['',"ایرانی","فرنگی","ایرانی و فرنگی"]} setvaluehandller={setservice}>سرویس</Select>
            <CheckBox val={pool} changeval={setpool} >استخر</CheckBox>
            <CheckBox val={pasio} changeval={setpasio} >پاسیو</CheckBox>
            <CheckBox val={Jacuzzi} changeval={setJacuzzi} >جکوزی</CheckBox>
            <CheckBox val={labi} changeval={setlabi} >لابی</CheckBox>
            <CheckBox val={conferencehall} changeval={setconferencehall} >سالن اجتماعات</CheckBox>
            <div className='selectbox'>
                    <label className='label'>   توضیحات برای گویا</label>
                    <textarea value={GoooyaExplain} className='inputfield-textarea' 
                    onChange={e=>setGoooyaExplain(e.target.value)}></textarea>
                    </div>
            </div>
            <div className='rstb-field'>
            <Select selectRes={innerWidth < 1250?true:false} val={coolerSystem} array={["","کولر","هواساز","فاقد"]} setvaluehandller={setcoolerSystem}> سیستم سرمایشی </Select>
            <Select selectRes={innerWidth < 1250?true:false} val={heaterSystem} array={["","شوفاژ","هواساز","پکیج","بخاری"]} setvaluehandller={setheaterSystem}> سیستم گرمایشی </Select>
            <Select selectRes={innerWidth < 1250?true:false} val={propertySituation} array={["","تخلیه","در دست مالک","در دست مستاجر"]} setvaluehandller={setpropertySituation}> وضعیت اسکان ملک </Select>
            <Select selectRes={innerWidth < 1250?true:false} val={documentSituation} array={["","شخصی","تعاونی","اوقافی","زمین شهری","قولنامه ای"]} setvaluehandller={setdocumentSituation}> وضعیت سند </Select>
            <Select selectRes={innerWidth < 1250?true:false} val={documentOnership} array={["","شش دانگ","مشاعی"]} setvaluehandller={setdocumentOnership}> مالکیت سند </Select>
            <Select selectRes={innerWidth < 1250?true:false} val={entry} array={["","از حیاط","از خیابان","از محوطه"]} setvaluehandller={setentry}> ورودی ملک </Select>
            <Select selectRes={innerWidth < 1250?true:false} val={flooring} array={['','پارکت','سرامیک','موزاییک']} setvaluehandller={setflooring}>کفپوش</Select>
            
           
            
           
           <div className='btn-2-box'>
            <button className='send' onClick={()=>setnumpage(1)}>بازگشت</button>
            {role?limitrole.includes(role.role)?<button className='send' 
            onClick={submitHandller}>صفحه بعد</button>:null:null}
            {role?!limitrole.includes(role.role)? <button className='send2' 
            onClick={submitHandller}>{!loading?"ارسال":<>درحال ارسال<span className="spin">
            </span></>}</button>:null:null}
                </div>
            </div>

        </div>
    )
}


export default Fields2;


 